<?php


class Cart
{
    public static function addProduct($id)
    {
        $id = intval($id);

        // Пустой массив для товаров в корзине
        $productsInCart = array();

        // Если в корзине уже есть товары (они хранятся в сессии)
        if (isset($_SESSION['product'])) {
            // То заполним наш массив товарами
            $productsInCart = $_SESSION['product'];
        }

        // Если товар есть в корзине, но был добавлен еще раз, увеличим количество
        if (array_key_exists($id, $productsInCart)) {
            $productsInCart[$id] ++;
        } else {
            // Добавляем нового товара в корзину
            $productsInCart[$id] = 1;
        }

        $_SESSION['product'] = $productsInCart;

        return self::countItems();
    }

    public static function countItems()
    {
        if (isset($_SESSION['product'])) {
            $count = 0;
            foreach ($_SESSION['product'] as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }

    public static function getProducts()
    {
        if(isset($_SESSION['product']))
        {
            return $_SESSION['product'];
        }
        return false;
    }

    public static function getTotalPrice($products)
    {
        $productsInCart=self::getProducts();

        $total=0;

        if($productsInCart)
        {
            foreach($products as $item)
            {
                $total+=$item['price']*$productsInCart[$item['id']];
            }
        }
        return $total;
    }
    public static function clear()
    {
        if (isset($_SESSION['product'])) {
            unset($_SESSION['product']);
        }
    }
    public static function deleteProduct($id)
    {
        $productsInCart = $_SESSION['product'];
        if(array_key_exists($id, $productsInCart)) {
            $productsInCart[$id]--;
        }
        if($productsInCart[$id]<=0){
            unset($productsInCart[$id]);
        }
        $_SESSION['product'] = $productsInCart;
        return self::countItems();
    }
}