<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section id="team" class="pb-5">
<div class="container">
    <p class="section-title h4">Цветочный бутик BARBARIS предоставляет весь спектр услуг по оформлению мероприятий.</p>
    <div class="section-header-underline"></div>
    <div class="row">
            <div class="video-gallery">
                <?php foreach ($offers as $offerItem):?>
                <div class="gallery-item">
                    <img src="/img/home/season_offers/<?php echo $offerItem['image'];?>.jpg" alt="" />
                    <div class="gallery-item-caption">
                        <div>
                            <h2><?php echo $offerItem['description'];?></h2>
                            <p><?php echo $offerItem['for'];?></p>
                        </div>
                        <a class="vimeo-popup" href="/season/<?php echo $offerItem['id'];?>/<?php echo $offerItem['id'];?>"></a>
                    </div>
                </div>
                <?php endforeach;?>
            </div>
    </div>
</div>
</section>
<!--Подвал-->
<?php
include('views/include/footer.php');
?>
<style>
    section {
        padding: 80px 0;
    }
    .section-title {
        text-align: center;
        color: #007b5e;
        margin-bottom: 50px;
        text-transform: uppercase;
    }
    .section-header-underline
    {
        border: 1px solid #222;
        width: 3rem;
        margin: 0 auto;
        margin-bottom: 30px;
    }
    /*Карточки*/
    .video-gallery {
        position: relative;
        margin: 0 auto;
        max-width: 1000px;
        text-align: center;
    }

    .video-gallery .gallery-item {
        position: relative;
        float: left;
        overflow: hidden;
        margin: 10px 1%;
        min-width: 320px;
        max-width: 580px;
        max-height: 360px;
        width: 48%;
        background: #000;
        cursor: pointer;
    }

    .video-gallery .gallery-item img {
        position: relative;
        display: block;
        opacity: .45;
        width: 105%;
        height: 300px;
        transition: opacity 0.35s, transform 0.35s;
        transform: translate3d(-23px, 0, 0);
        backface-visibility: hidden;
    }

    .video-gallery .gallery-item .gallery-item-caption {
        padding: 2em;
        color: #fff;
        text-transform: uppercase;
        font-size: 1.25em;
    }

    .video-gallery .gallery-item .gallery-item-caption,
    .video-gallery .gallery-item .gallery-item-caption > a {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .video-gallery .gallery-item h2 {
        font-weight: 300;
        overflow: hidden;
        padding: 0.5em 0;
    }


    .video-gallery .gallery-item h2,
    .video-gallery .gallery-item p {
        position: relative;
        margin: 0;
        z-index: 10;
    }

    .video-gallery .gallery-item p {
        letter-spacing: 1px;
        font-size: 68%;

        padding: 1em 0;
        opacity: 0;
        transition: opacity 0.35s, transform 0.35s;
        transform: translate3d(10%, 0, 0);
    }

    .video-gallery .gallery-item:hover img {
        opacity: .3;
        transform: translate3d(0, 0, 0);

    }

    .video-gallery .gallery-item .gallery-item-caption {
        text-align: left;
    }

    .video-gallery .gallery-item h2::after {
        content: "";
        position: absolute;
        bottom: 0;
        left: 0;
        width: 15%;
        height: 1px;
        background: #fff;

        transition: transform 0.3s;
        transform: translate3d(-100%, 0, 0);
    }

    .video-gallery .gallery-item:hover h2::after {
        transform: translate3d(0, 0, 0);
    }

    .video-gallery .gallery-item:hover p {
        opacity: 1;
        transform: translate3d(0, 0, 0);
    }
    @media screen and (max-width: 50em) {
        .video-gallery .gallery-item {
            display: inline-block;
            float: none;
            margin: 10px auto;
            width: 100%;
        }
    }

</style>