<?php
include_once ('models/SeasonOffersCategory.php');
class SeasonController
{
     public function actionIndex($id)
     {
         $connection=Db::connect();
         $offers=SeasonOffersCategory::getSeasonCategory($connection,$id);
         if(!$offers) header('Location:/404');
         require_once('views/season_offers/season_offers.php');
         return true;
     }
     public function actionView($id)
     {
         $connection=Db::connect();
         $season=SeasonOffersCategory::getSeason($connection,$id);
         if(!$season) header('Location:/404');
         require_once('views/season_offers/season_about.php');
         return true;
     }
}