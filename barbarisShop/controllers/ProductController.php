<?php
include_once ('models/Category.php');
include_once ('models/Product.php');
class ProductController
{

    public function actionIndex($page=1)
    {
        $connection=Db::connect();
        $categoryList=Category::getCategoriesList($connection);
        $productList=Product::getLatestProducts($connection,$page);
        $numFlowersOnThePage=Product::SHOW_BY_DEFAULT;
        $numPages = ceil(Product::getTotalProduct($connection)['count'] / $numFlowersOnThePage);
        require_once ('views/product/product.php');
        return true;
    }
    public function actionView($id)
    {
        $connection=Db::connect();
        $categoryList=Category::getCategoriesList($connection);
        $productId=Product::getProductById($connection,$id);
        if(!$productId) header('Location:/404');
        require_once ('views/product/aboutProduct.php');
        return true;
    }
    public function actionAll($page=1)
    {
        $connection=Db::connect();
        $categoryList=Category::getCategoriesList($connection);
        $productList=Product::getLatestProducts($connection,$page);
        $numFlowersOnThePage=Product::SHOW_BY_DEFAULT;
        $numPages = ceil(Product::getTotalProduct($connection)['count'] / $numFlowersOnThePage);
        require_once ('views/product/product.php');
        return true;
    }

    public function actionSorti($page=1)
    {
        $connection=Db::connect();
        $categoryList=Category::getCategoriesList($connection);
        $productList=Product::Sort($connection, $page);
        $numFlowersOnThePage=Product::SHOW_BY_DEFAULT;
        $numPages = ceil(Product::getTotalProduct($connection)['count'] / $numFlowersOnThePage);
        require_once ('views/product/sort.php');
        return true;
    }

    public function actionSortd($page=1)
    {
        $connection=Db::connect();
        $categoryList=Category::getCategoriesList($connection);
        $productList=Product::Sortd($connection, $page);
        $numFlowersOnThePage=Product::SHOW_BY_DEFAULT;
        $numPages = ceil(Product::getTotalProduct($connection)['count'] / $numFlowersOnThePage);
        require_once ('views/product/sort.php');
        return true;
    }
}