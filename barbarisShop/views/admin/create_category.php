<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section class="product">
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li><a href="/admin/category" class="cabinet">Управление категориями</a></li>
                    <li class="active cabinet" >Добавить категорию</li>
                </ol>
            </div>
            <div class="col-12 title">
                <h4>Добавить новую категорию</h4>
            </div>

            <br/>
            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="col-12 title">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">
                        <table>
                            <tr>
                                <td><label for="category_name">Название</label></td>
                                <td><input type="text" name="name" placeholder="" value="" id="category_name"></td>
                            </tr>

                            <tr>
                                <td><label for="category_number">Порядковый номер</label></td>
                                <td><input type="text" name="sort_order" placeholder="" value="" id="category_number"></td>
                            </tr>

                            <tr>
                                <td><label for="category_status">Статус</label></td>
                                <td>
                                    <select name="status">
                                        <option value="1" selected="selected">Отображается</option>
                                        <option value="0">Скрыта</option>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <input type="submit" name="submit" class="btn btn-default save" value="Сохранить">
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>

<style>
    body{
        background: #eee !important;
    }

    .product
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .title
    {
        display: flex;
        justify-content: center;
    }
    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        font-size: 14px;
        background: white;
        padding: 10px;
    }
    .save
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        border-radius: 10px;
        color:  black;
        background-color: white;
        background-image:
                -webkit-repeating-linear-gradient(45deg, lightgray, lightgray 1px, rgba(255,0,0,0) 2px, rgba(255,0,0,0) 10px),
                -webkit-repeating-linear-gradient(135deg, lightgray, lightgray 1px, rgba(255,0,0,0) 2px, rgba(255,0,0,0) 10px);
        box-shadow: 2px 2px 3px rgba(0,0,0,0.3);
    }
    .save:hover
    {
        background-image:
                -webkit-repeating-linear-gradient(left, lightgray, lightgray 1px, rgba(255,0,0,0) 2px, rgba(255,0,0,0) 10px);
    }
</style>