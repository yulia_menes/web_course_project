<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section class="product">
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li class="active cabinet">Управление товарами</li>
                </ol>
            </div>

            <div class="col-12 create">
            <a href="/admin/product/create" class="btn btn-default back createBtn"><i class="fa fa-plus">Добавить товар</i></a>
            </div>
            <div class="col-12 list">
            <h4>Список товаров</h4>
            </div>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID товара</th>
                    <th>Артикул</th>
                    <th>Название товара</th>
                    <th>Цена</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($productsList as $product):?>
                <tr>
                    <td><?php echo $product['id'];?></td>
                    <td><?php echo $product['code'];?></td>
                    <td><?php echo $product['name'];?></td>
                    <td><?php echo $product['price'];?></td>
                    <td><a href="/admin/product/update/<?php echo $product['id'];?>" title="Редактировать">
                            <i class="fa fa-pencil"></i>
                        </a>
                    </td>
                    <td><a href="/admin/product/delete/<?php echo $product['id'];?>" title="Удалить">
                            <i class="fa fa-times"></i>
                        </a>
                    </td>
                </tr>
                <?php endforeach;?>
            </table>

        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>

<style>
    body{
        background: #eee !important;
    }

    .product
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .create, .list
    {
        padding-top: 20px;
        display: flex;
        justify-content: center;

    }
    .createBtn
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        border-radius: 10px;
        color: black;
        background-color: white;
        background-image: -webkit-radial-gradient(white 2px, lightgray 2px);
        background-size: 12px 12px;
        box-shadow: 2px 2px 3px rgba(0,0,0,0.3);
    }
    .createBtn:hover
    {
        background-size: 16px 16px;
        color: black;
    }

    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        font-size: 14px;
        background: white;
        padding: 10px;
    }
    .fa
    {
        color:black;
    }
</style>