<?php

//FRONT CONTROLLER

// 1. Общие настройки
//отображение ошибок - ОЧЕНЬ ЖЕЛАТЕЛЬНО ВЫКЛЮЧИТЬ В КОНЦЕ!!
//ini_set('display_errors',1);
//error_reporting(E_ALL);


//define('ROOT', dirname(__FILE__));


/*// сбросить все переменные сессии
$_SESSION = array();

// сбросить куки, к которой привязана сессия
if (ini_get("session.use_cookies")) {
    $params = session_get_cookie_params();
    setcookie(session_name(), '', time() - 42000,
        $params["path"], $params["domain"],
        $params["secure"], $params["httponly"]
    );
}

// уничтожить сессию
session_destroy();
*/

session_start();

ini_set('display_errors', 0);

// 2. Подключение файлов системы
require_once ('./components/Router.php');
include_once ('components/Cart.php');
include_once ('models/User.php');
include_once ('components/Db.php');

// 3. Установка соединения с БД


// 4. Вызов Router
$router=new Router();
$router->run();