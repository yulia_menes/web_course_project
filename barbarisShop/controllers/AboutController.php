<?php
include_once ('models/Team.php');
class AboutController
{
    public function actionIndex()
    {
        $connection=Db::connect();
        $team=Team::getTeamList($connection);
        require_once('views/about/about.php');
        return true;
    }
}