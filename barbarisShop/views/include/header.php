<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Подключаем Bootstrap CSS -->
    <link rel= "stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <title>BARBARIS</title>
</head>
<body>
<div class="container p-0 sticky-top">
    <nav class="navbar navbar-expand-lg navbar-dark teal mb-4">
        <a class="navbar-brand" href="/">BARBARIS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="/home" id="menu">ГЛАВНАЯ<span class="sr-only">(current)</span></a>
                </li>
               <li class="nav-item">
                    <a class="nav-link" href="/product" id="menu">АССОРТИМЕНТ</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="/home#gallery" id="menu">ГАЛЕРЕЯ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about" id="menu">О НАС</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/delivery" id="menu">ДОСТАВКА</a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item ">

                    <a class="nav-link" href="/cart" id="menu">
                        <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                        КОРЗИНА
                        <span id="cart-count">(<?php echo Cart::countItems();?>)</span>
                        <span class="sr-only">(current)</span></a>
                </li>
                <?php if(User::isGuest()):?>
                <li class="nav-item ">

                    <a class="nav-link" href="/user/login" id="menu">
                        <i class="fa fa-lock" aria-hidden="true"></i>
                        ВХОД
                        <span class="sr-only">(current)</span></a>
                </li>
                <?php else: ?>
                <li class="nav-item ">

                    <a class="nav-link" href="/cabinet" id="menu">
                        <i class="fa fa-user" aria-hidden="true"></i>
                       АККАУНТ
                        <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item ">

                    <a class="nav-link" href="/user/logout" id="menu">
                        <i class="fa fa-unlock" aria-hidden="true"></i>
                        ВЫХОД
                        <span class="sr-only">(current)</span></a>
                </li>
                <?php endif;?>
            </ul>
        </div>
    </nav>
</div>
<style>
    nav{
        background-color: darkgray!important;
    }
    .nav-link
    {
        font-size: 14px;
    }
    .navbar
    {
        position:absolute;
        top:0px;
        z-index:10;
        width: 100%;
    }
    .navbar .dropdown-menu a:hover {
        color: black !important;
    }
    #menu{
        color: white;
        letter-spacing: 1px;
        border-bottom: 1px solid transparent;
        border-top: 1px solid transparent;
    }
    #menu:hover {
        border-bottom: 1px solid white;
        border-top: 1px solid white;
    }
    #menu:focus
    {
        background-color: black;
    }
</style>
<script>
    $(document).ready(function(){
        $(".add-to-cart").click(function () {
            var id = $(this).attr("data-id");
            $.post("/cart/addAjax/"+id, {}, function (data) {
                $("#cart-count").html('('+data+')');
            });
            return false;
        });
    });
</script>