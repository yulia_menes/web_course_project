<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<?php
include('views/include/carousel.php');
?>

<?php
include('views/include/cards.php')
?>
<!-- Изображение-->
<div class="imgWrapper" data-src="img/home/image.jpg"></div>
<!--Услуги-->
<div class="container-fluid background">
    <div class="row">
        <h3 class="h">Что вы получаете заказывая букеты и услуги в BARBARIS</h3>
    </div>
    <div class="row icons">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 flower ico1">

                 <img src="img/home/flower.ico" class="ico">

                <p class="service">Всегда самые свежие и красивые цветы<br> из разных уголков мира.</p>

            </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 flower">

            <img src="img/home/surprise.ico" class="ico">
            <p class="service">Разработка и изготовление уникальных<br> корпоративных подарков.</p>

        </div>
    </div>
    <div class="row icons">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 flower ico1">

            <img src="img/home/heart.ico" class="ico">


            <p class="service">Делаем букеты с любовью и нежностью<br> для наших клиентов!</p>

        </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 flower">

            <img src="img/home/rose.ico" class="ico">


            <p class="service">Флористический декор интерьера и комплексное<br> оформление мероприятий.</p>

        </div>
    </div>
    <div class="row icons">
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 flower ico1">

            <img src="img/home/star.ico" class="ico">

            <p class="service">Каждый букет от BARBARIS уникален.<br> Мы трепетно подбираем каждую деталь<br> для идеального букета.</p>

            </div>
        <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 flower">

            <img src="img/home/clock.ico" class="ico">

            <p class="service">Ценим Ваше время. Доставка 24/7.<br> Привезём Ваш заказ вовремя!</p>
        </div>
    </div>
</div>

<!--Философия-->

<div class="container-fluid blok3">
    <div class="row">
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 flora">
        <h3 class="h">Философия цветочного бутика BARBARIS</h3>
        <p>Для нас цветы - это часть истории. Эта история о любви, о благодарности, надежде, радости, счастье, о Вас. Каждый букет трепетно созданный нашими флористами будет отображать Вашу историю. Мы искренне верим в то, что делаем мир красивее, а наших клиентов чуточку счастливее.</p>
        </div>
        <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 flora">
            <img src="img/home/filosofics.jpg" class="filosof">
        </div>
    </div>

</div>

<!--Галерея-->
<div class="container-fluid gallery" id="gallery">

        <h3 class="h">Галерея наших робот</h3>
        <p>В цветочном бутике BARBARIS вы сможете не только<br>
            подобрать букет для любого случая, но и заказать<br>
            флористический декор. Мы с радостью поможем удивить<br>
            Ваших близких, а также подскажем как стильно и<br>
            элегантно оформить торжество.</p>
    <div class="row g">
        <?php foreach ($gallery as $galleryItem):?>
        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
            <a href="/gallery/<?php echo $galleryItem['id'];?>" class="gal"><img src="img/home/<?php echo $galleryItem['image'];?>.jpg" class="gallery1">
                <br/>
            <p class="gallery1_1"><?php echo $galleryItem['name'];?></p>
            </a>
        </div>
        <?php endforeach;?>

    </div>

</div>
<?php
include('views/include/reviews.php');
?>

<!--Изображение-->
<div class="imgWrapper" data-src="img/home/image1.jpg"></div>

<!--Подвал-->
<?php
include('views/include/footer.php');
?>


<style>
/*Изображение*/
    .imgWrapper
    {
        height:10px;
        background-size: cover;
        background-position: center;
        background-attachment: fixed;
        background-repeat: no-repeat;
        position: relative;
        padding-top: calc(100% / 21 * 9);
    }

  /*Услуги*/
.ico
{
    height: 70px;
}


.ico1
{

    display: flex;
    align-items:center;
    justify-content: flex-end;
}

.flower
{
    display: flex;
    padding-top: 25px;
    align-items: center;
}
p
{
    font-weight: 100;
    font-size: 20px;
    font-family: 'Open Sans',Arial,sans-serif;
}
.service
{
   padding-left: 15px;
    padding-top: 10px;
}
.background
{
    padding-top: 5px;
    background-color: lavenderblush;

}  
/*Флористика*/
.flora
{
    text-align: center;
    align-items: end;

}
.filosof
{
    display: block;
    margin-left: auto;
    margin-right: auto;
    height: 310px;
}
.blok3
{
    padding-top: 15px;
    padding-bottom: 15px;
    background-color: white;
}
/*Галерея*/
.gallery
{
    text-align: center;
    padding-top: 30px;
    background-color: lavenderblush;
}
.gallery .h
{
    font-weight: 100;
    font-size: 38px;
    font-family: 'Open Sans',Arial,sans-serif;
    margin-top: 20px;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}
.g
{
    padding-top: 30px;
}
.gallery1
{
    height: 500px;
}
.gal{
    display: inline-block;
    text-decoration: none !important;
    color:black;
    text-transform: uppercase;
    font-size: 20px;
    font-weight: bold;
    font-family: 'Montserrat', sans-serif;
    transition: 0.4s ease-in-out;
}
.gal:hover
{
    color:black;
}


</style>

<script>
    /*Изображение*/
    const IMAGE_CONTROLLER = (function() {
        const DOM = {
            imgWrapperElArr: Array.from(document.querySelectorAll('.imgWrapper')),
        }
        let imgUrl;

        let setupImages = function() {
            DOM.imgWrapperElArr.forEach(imgWrapper => {
                imgUrl = imgWrapper.getAttribute('data-src');
                imgWrapper.style.backgroundImage = `url(${imgUrl})`;
            })
        }

        return {
            init() {
                setupImages();
            }
        }
    })();

    IMAGE_CONTROLLER.init();
   
  
</script>




















</body>
</html>
