<?php

class Router
{
private $routes; //массив, в котором будут храниться маршруты

public function __construct()
{
     $this->routes=include('config/routes.php');
}

//Метод возвращает строку
private function getURI()
{
    if(!empty($_SERVER['REQUEST_URI']))
    {
        return trim($_SERVER['REQUEST_URI'],'/');
    }
}


public function run()
{
    // Получить строку запроса
    $uri=$this->getURI();
    // Проверить наличие такого запроса в routes.php
    foreach ($this->routes as $uriPattern=>$path){

        //Сравниваем $uriPattern и $uri
        if(preg_match("~$uriPattern~", $uri)) // ~ - розделитель вместо /, потому что в адересе могут сожержаться /
        {


            //Получаем внутренний путь из внешнего согласно правилк
            $internalRoute=preg_replace("~$uriPattern~",$path,$uri);


            //Определить какой контроллер
            //и action обробатывает запрос

            $segments = explode('/', $internalRoute);//разделить на две части

            $controllerName=array_shift($segments).'Controller';//получает значение первого элемента в
            //и удаляет его из массива. К этому значению добавляем слово Controller
            $controllerName=ucfirst($controllerName);//функция делает первою букву строки заглавной

            $actionName='action'.ucfirst(array_shift($segments));

            $parameters=$segments;

            //Подключить файл класса-контроллера

            if(file_exists('controllers/'.$controllerName.'.php')) {
                include_once('controllers/'.$controllerName.'.php');
            }


            //Создать обьект, вызвать метод (т.е. action)
            $controllerObject=new $controllerName;
            $result=call_user_func_array(array($controllerObject, $actionName),$parameters);
            if($result!=null){
                break;
            }


        }
    }


}
}