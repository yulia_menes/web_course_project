<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<div class="container features_items ">
    <h2>Каталог</h2>
    <div class="row ">
        <?php foreach ($categories as $categoryItem):?>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                <h4><a class="category <?php if($categoryId==$categoryItem['id']) echo 'active'?>"
                       href="/category/<?php echo $categoryItem['id'];?>">
                        <?php echo $categoryItem['name'];?>
                    </a>
                </h4>
            </div>
        <?php endforeach;?>
    </div>
    <h2>Все товары</h2>
    <div class="row text-center">
        <?php foreach ($categoryProducts as $productItem):?>
            <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
                <?php if($productItem['is_new']):?>
                    <img src="/img/product/new1.png" class="new" alt="">
                <?php endif;?>
                <a href="/product/<?php echo $productItem['id'];?>" class="name">
                <img src="<?php echo Product::getImage($productItem['id']); ?>" width="350"  alt="" class="img"/>
                <h6>


                        <?php echo $productItem['name'];?>
                    </a>
                </h6>
                <p><b><?php echo $productItem['price'];?> грн</b></p>
                <a href="/cart/add/<?php echo $productItem['id'];?>" class="btn btn-default basket add-to-cart" data-id="<?php echo $productItem['id'];?>">
                    <span>→</span>
                    <i class="fa fa-shopping-basket" aria-hidden="true"></i>
                    В корзину
                    <span>←</span>
                </a>

            </div>

        <?php endforeach;?>
        <!-- Вивід посторінково -->
        <?php if ($numPages > 1): ?>
            <ul class="pagination" id="pages">
                <li class="page-item <?php if ($page - 1 < 1) echo 'disabled' ?>">
                    <a class="page-link"
                       href="http://barbarisshop/category/<?php echo $categoryId?>/page-<?php echo $page - 1 ?>"
                       aria-label="Previous">
                        <span aria-hidden="true">&lt;</span>
                    </a>
                </li>
                <?php for ($i = $page - 2; $i <= $page + 2; $i++): ?>

                    <?php if ($i <= $numPages && $i > 0): ?>
                        <li class="page-item <?php if ($i == $page) echo 'active'; ?>">
                            <a class="page-link"
                               href="http://barbarisshop/category/<?php echo $categoryId ?>/page-<?php echo $i ?>">
                                <?php echo $i ?> </a></li>
                    <?php endif; ?>
                <?php endfor; ?>
                <li class="page-item <?php if ($page + 1 > $numPages) echo 'disabled' ?>">
                    <a class="page-link"
                       href="http://barbarisshop/category/<?php echo $categoryId ?>/page-<?php echo $page + 1 ?>"
                       aria-label="Next">
                        <span aria-hidden="true">&gt;</span>
                    </a>
                </li>
            </ul>
        <?php endif; ?>


    </div>
</div>
<!--Подвал-->
<?php
include('views/include/footer.php');
?>





<style>
    /*Категории*/

    .features_items
    {
        padding-top: 100px;
        text-align: center;
    }


    .category
    {

        display: flex;
        align-items: center;
        justify-content: center;
        height: 80px;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        padding: 15px 20px;
        font-size: 15px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        /*border-radius: 10px;*/
        border-style: dashed;
        color: black;
        /*background: darkgray;*/
        /*border-bottom: 5px solid black;*/
        box-shadow: 3px 3px 3px rgba(0,0,0,0.3);
    }
    .category:hover
    {
        color: black;
        text-decoration: none;
        box-shadow: 0 1px 2px rgba(0,0,0,0.3);
        text-shadow: 0 1px 0 rgba(0,0,0,0.3);
    }
    /*Товары*/
    .img
    {

        height: 500px;
    }
    .new
    {
        height: 50px;
        position:absolute;
        margin-top: -15px;
    }
    product
    {
        padding-top: 100px;
        text-align: center;
    }
    .basket
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;

    }
    .basket
    {
        color: darkgreen;
        border-radius: 25px;
        border: 3px solid darkgreen;
    }
    .basket:hover
    {
        color: black;
        background: lightgray;
    }
    .basket span
    {
        opacity: 0;
        padding-left: 5px;
        padding-right: 5px;
        font-weight: bold;
        transition: 0.4s ease-in-out;
    }
    .basket:hover span
    {
        opacity: 1;
        padding-left: 10px;
        padding-right: 10px;
        color: black;
    }
    .name
    {
        text-decoration: none;
        color:black;
    }
    .name:hover
    {
        color: darkgreen;
    }
    .active
    {
        background-color: darkgray;
    }
    .pagination
    {
        margin-left: auto;
        margin-right: auto;
    }
    .page-item
    {
        background-color: darkgrey;
    }


</style>
