<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<div class="container features_items">
    <h2>Каталог</h2>
    <div class="row ">
        <?php foreach ($categoryList as $categoryItem):?>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                <h4><a class="category"
                       href="/category/<?php echo $categoryItem['id'];?>">
                        <?php echo $categoryItem['name'];?>
                    </a>
                </h4>
            </div>
        <?php endforeach;?>
    </div>
    <div class="row about">
        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
            <?php if($productId['is_new']):?>
                <img src="/img/product/new1.png" class="new" alt="">
            <?php endif;?>
            <img src="<?php echo Product::getImage($productId['id']); ?>" width="350" alt="" class="img"/>


        </div>

        <div class="col-xl-6 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="product-information">
                <h4><?php echo $productId['name'];?></h4>
                <h5>Код товара:<?php echo $productId['code'];?></h5>
                    <h5 class="price"><?php echo $productId['price'];?> грн</h5>
                <a href="#" data-id="<?php echo $productId['id']; ?>"
                   class="btn btn-default add-to-cart">
                    В корзину
                </a>
                <hr/>

                        <div class="card text-center my-4">
                            <!-- navigation in .card-header -->
                            <div class="card-header">
                                <ul class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#tab1">Важная информация</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab2">Характеристики</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#tab3">Описание</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- .card-body.tab-content  -->
                            <div class="card-body tab-content">
                                <div class="tab-pane fade show active" id="tab1">
                                    <p>
                                        <u>
                                            Наличие:
                                        </u>
                                        <?php if($productId['avallability']):?>
                                            На складе
                                        <?php endif;?>
                                        <?php if(!($productId['avallability'])):?>
                                            Нет в наличии
                                        <?php endif;?>
                                    </p>
                                    <p>
                                        <u>
                                            Состояние:
                                        </u>
                                        <?php if($productId['is_new']):?>
                                            Новинка
                                        <?php endif;?>
                                        <?php if(!($productId['is_new'])):?>
                                            Проверено временем)
                                        <?php endif;?>
                                    </p>
                                </div>
                                <div class="tab-pane fade" id="tab2">

                                    <p><u class="u">Событие:</u> <?php echo $productId['event'];?></p>

                                    <p><u class="u">Кому:</u> <?php echo $productId['whom'];?></p>
                                </div>
                                <div class="tab-pane fade" id="tab3">
                                    <p><u class="u">Состав:</u> <?php echo $productId['sostav'];?></p>
                                    <p><u class="u">Цвет:</u> <?php echo $productId['color'];?></p>
                                    <p><u class="u">Цветы:</u> <?php echo $productId['flower'];?></p>
                                    <p><u class="u">Внимание:</u> букет внешне может отличаться от изображения на сайте, состав остается без изменений.</p>
                                </div>
                            </div>
        </div>
    </div>

</div>

        <!--Подвал-->
        <?php
        include('views/include/footer.php');
        ?>



<style>
    .features_items
    {
        padding-top: 100px;
        text-align: center;
    }


    .category
    {

        display: flex;
        align-items: center;
        justify-content: center;
        height: 80px;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        padding: 15px 20px;
        font-size: 15px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        /*border-radius: 10px;*/
        border-style: dashed;
        color: black;
        /*background: darkgray;*/
        /*border-bottom: 5px solid black;*/
        box-shadow: 3px 3px 3px rgba(0,0,0,0.3);
    }
    .category:hover
    {
        color: black;
        text-decoration: none;
        box-shadow: 0 1px 2px rgba(0,0,0,0.3);
        text-shadow: 0 1px 0 rgba(0,0,0,0.3);
    }
    .new
    {
        height: 50px;
        position:absolute;
        margin-top: -15px;
    }


    .about
    {
        padding-top: 50px;
    }
    p
    {
        font-weight: 100;
        font-size: 15px;
        font-family: 'Open Sans',Arial,sans-serif;
    }

    .btn
    {
        position: relative;
        height: 40px;
        line-height: 40px;
        padding: 0 30px 0 40px;
        margin: 0 20px;
        background: linear-gradient(-135deg, gray, darkgray);

        border-width: 0;
        border-radius: 50px;
        font-family: 'Montserrat Alternates', sans-serif;
        font-size: 14px;
        color: black;
        outline: none;
        cursor: pointer;
    }
    .btn:before
    {
        content: "\f067";
        position: absolute;
        left: 5px;
        top: 5px;
        font-family: FontAwesome;
        color: white;
        width: 30px;
        height: 30px;
        line-height: 30px;
        text-align: center;
        border-radius: 50%;
        background: transparent;
        transition: .3s ease-in-out;
    }
    .btn:hover
    {
        color:black;
    }
    .btn:hover:before {
        background: white;
        color: black;
    }
    .card
    {
        height: 317px;
    }
    .card-header
    {
        background-color: gray;
    }
    .nav-link
    {
        color: white;
    }
    .nav-link:hover
    {
        color: white;
    }
    .img
    {
        height:500px;
    }
    .card-body
    {
        text-align: left;
    }
    .price
    {
        color:darkgreen;
    }



</style>
<script>



</script>