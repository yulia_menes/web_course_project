<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section>
    <div class="container delete_product">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li><a href="/admin/order" class="cabinet">Управление заказами</a></li>
                    <li class="active cabinet" >Удалить заказ</li>
                </ol>
            </div>
            <div class="col-12 delete">
                <h4>Удалить заказ №<?php echo $id;?></h4>
            </div>
            <div class="col-12 delete">
                <h5>Вы действительно хотите удалить этот заказ?</h5>
            </div>
            <div class="col-12 delete">
                <form method="post">
                    <input type="submit" name="submit" value="Удалить" class="button">
                </form>
            </div>
        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>

<style>
    body{
        background: #eee !important;
    }

    .delete_product
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .delete
    {
        display: flex;
        justify-content: center;
    }
    .button
    {
        display: flex;
        justify-content: center;
        vertical-align: center;
        width: 140px;
        height: 45px;
        line-height: 45px;
        border-radius: 45px;
        margin: 10px 20px;
        font-family: 'Montserrat', sans-serif;
        font-size: 11px;
        text-transform: uppercase;
        text-align: center;
        letter-spacing: 3px;
        font-weight: 600;
        color: #524f4e;
        background: white;
        box-shadow: 0 8px 15px rgba(0,0,0,.1);
        transition: .3s;
    }
    .button:hover
    {
        background: gray;
        box-shadow: 0 15px 20px black;
        color: white;
        transform: translateY(-7px);
    }
</style>
