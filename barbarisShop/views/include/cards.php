<!--Карточки-->
<div class="container-fluid">
    <div class="row">
        <h3 class="h">Сезонное предложение</h3>
    </div>
    <div class="card-group">
        <?php foreach ($seasonOffersCategory as $seasonItem):?>
        <div class="card text-center">
            <img class="card-img-top card1" src="img/home/season_offers/<?php echo $seasonItem['image'];?>.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title"><?php echo $seasonItem['description'];?></h5>
                <a href="/season/<?php echo $seasonItem['id'];?>" class="see">Посмотреть</a>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>

<style>

    .row .h
    {
        font-weight: 100;
        font-size: 38px;
        font-family: 'Open Sans',Arial,sans-serif;
        margin-top: 20px;
        margin-left: auto;
        margin-right: auto;
        text-align: center;
    }
    .card-title
    {
        font-weight: 100;
        font-size: 20px;
        font-family: 'Open Sans',Arial,sans-serif;
    }
.card
{
    border:none;
}
    .see{
        width: 125px;
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;

        padding: 15px 20px;
        font-size: 10px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        border-radius: 10px;
        color: #40382D;
        box-shadow: inset 0 -5px 0 #40382D;
    }
    .see:hover
    {
        box-shadow: inset 0 -60px 0 #40382D, 2px 2px 3px rgba(0,0,0,0.3);
        color: #DECDA5;
    }

</style>
