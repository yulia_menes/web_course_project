<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>

<div class="container features_items ">
    <h2>Каталог</h2>
    <div class="row ">
        <?php foreach ($categories as $categoryItem):?>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                <h4><a class="category" href="/category/<?php echo $categoryItem['id'];?>">
                        <?php echo $categoryItem['name'];?>
                    </a>
                </h4>
            </div>
        <?php endforeach;?>
    </div>
    <h2>Корзина</h2>
    <?php if ($result): ?>

        <h2>Заказ оформлен. Мы Вам перезвоним!</h2>

    <?php else: ?>

        <p>Выбрано товаров: <?php echo $totalQuantity; ?>, на сумму: <?php echo $totalPrice; ?> грн.</p><br/>

        <div class="col-12">

            <p>Для оформления заказа заполните форму. Наш менеджер свяжется с Вами.</p>
            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="login-form">
                <form action="#" method="post" class="transparent">
                    <div class="form-inner1">
                        <h3>Оформление заказа</h3>
                        <label for="userName">Ваше Имя</label>
                    <input type="text" name="userName" placeholder="" value="<?php echo $userName; ?>" required/>

                        <label for="userPhone">Номер телефона</label>
                    <input type="text" name="userPhone" placeholder="" value="<?php echo $userPhone; ?>" required/>

                        <label for="userComment">Комментарий к заказу</label>
                    <input type="text" name="userComment" placeholder="" value="<?php echo $userComment; ?>" required/>

                    <br/>
                    <br/>
                    <input type="submit" name="submit" class="btn btn-default" value="Оформить" />
                    </div>
                </form>
            </div>
        </div>

    <?php endif; ?>

</div>

</div>
</div>
<!--Подвал-->
<?php
include('views/include/footer.php');
?>

<style>
    body{
        background: #eee !important;
    }
    /*Категории*/

    .features_items
    {
        padding-top: 100px;
        text-align: center;

    }


    .category
    {

        display: flex;
        align-items: center;
        justify-content: center;
        height: 80px;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        padding: 15px 20px;
        font-size: 15px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        /*border-radius: 10px;*/
        border-style: dashed;
        color: black;
        /*background: darkgray;*/
        /*border-bottom: 5px solid black;*/
        box-shadow: 3px 3px 3px rgba(0,0,0,0.3);
    }
    .category:hover
    {
        color: black;
        text-decoration: none;
        box-shadow: 0 1px 2px rgba(0,0,0,0.3);
        text-shadow: 0 1px 0 rgba(0,0,0,0.3);
    }

    /*Форма*/
    .transparent {
        margin-right: auto;
        margin-left: auto;
        text-align: left;
        width: 800px;
        position: relative;
        max-width: 450px;
        padding: 60px 50px;
        /*margin: 50px auto 0;*/
        background-image: url(https://html5book.ru/wp-content/uploads/2017/01/photo-roses.jpg);
        background-size: cover;
    }
    .transparent:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: linear-gradient(to right bottom,rgba(43, 44, 78, .5),rgba(104, 22, 96, .5));
    }
    .form-inner1 {position: relative;}
    .form-inner1 h3 {
        position: relative;
        margin-top: 0;
        color: white;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 26px;
        text-transform: uppercase;
    }
    .form-inner1 h3:after {
        content: "";
        position :absolute;
        left: 0;
        bottom: -6px;
        height: 2px;
        width: 60px;
        background: #1762EE;
    }
    .form-inner1 label {
        display: block;
        padding-left: 15px;
        font-family: 'Roboto', sans-serif;
        color: rgba(255,255,255,.6);
        text-transform: uppercase;
        font-size: 14px;
    }
    .form-inner1 input {
        display: block;
        width: 100%;
        padding: 0 15px;
        margin: 10px 0 15px;
        border-width: 0;
        line-height: 40px;
        border-radius: 20px;
        color: white;
        background: rgba(255,255,255,.2);
        font-family: 'Roboto', sans-serif;
    }


    .form-inner1 input[type="submit"] {background: slateblue;}
    li {
        list-style-type: none; /* Убираем маркеры */
    }
</style>