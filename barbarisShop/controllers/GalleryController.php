<?php
include_once ('models/Gallery.php');
class GalleryController
{
    public function actionIndex($id)
    {
        $connection=Db::connect();
        $gallery=Gallery::getGalleryCategory($connection,$id);
        if(!$gallery) header('Location:/404');
        require_once('views/gallery/gallery.php');
        return true;
    }
}