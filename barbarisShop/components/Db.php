<?php


class Db
{
    public static function connect()
    {
        //Визначаємо параметри з'єднання
        $user="root";
        $password="root";
        $dsn="mysql:host=localhost;dbname=barbarisshop;port=3306;charset=utf8";
        //Створюємо керуючий об'єкт з'єднання
        $connection=new PDO($dsn, $user, $password);
        //Дані отримувати як асоціативні масиви
        $connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        //Повертаємо з'єднання в керуючий скрипт
        return $connection;
    }
}