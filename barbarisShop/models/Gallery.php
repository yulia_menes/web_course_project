<?php


class Gallery
{
    public static function getGallery(PDO $connection)
    {
        //Формулюємо SQL-запит
        $sql="SELECT * FROM gallery";
        //Готуємо запит до виконання
        $stmt=$connection->prepare($sql);
        //Виконуємо запит
        $stmt->execute();
        //Отримуємо результат запиту в масиві
        $gallery=$stmt->fetchAll();
        //Повертаємо масив в керуючий скрипт
        return $gallery;
    }
    public static function getGalleryCategory(PDO $connection, $id)
    {
        $id=intval($id);

        if($id)
        {
            $sql="SELECT * FROM gallery_category WHERE galery_category=".$id;
            //Готуємо запит до виконання
            $stmt=$connection->prepare($sql);
            //Виконуємо запит
            $stmt->execute();
            //Отримуємо результат запиту в масиві
            $galleryCategory=$stmt->fetchAll();
            //Повертаємо масив в керуючий скрипт
            return $galleryCategory;
        }
    }
}