<?php


class Team
{
public  static function getTeamList(PDO $connection)
{
    //Формулюємо SQL-запит
    $sql="SELECT * FROM team";
    //Готуємо запит до виконання
    $stmt=$connection->prepare($sql);
    //Виконуємо запит
    $stmt->execute();
    //Отримуємо результат запиту в масиві
    $teamList=$stmt->fetchAll();
    //Повертаємо масив в керуючий скрипт
    return $teamList;
}
}