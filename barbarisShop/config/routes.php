<?php
    return array(
        'admin/order/delete/([0-9]+)' => 'adminOrder/delete/$1', //actionDelete в AdminOrderController
        'admin/order/update/([0-9]+)' => 'adminOrder/update/$1', //actionUpdate в AdminOrderController
        'admin/order/view/([0-9]+)' => 'adminOrder/view/$1', //actionView в AdminOrderController
        'admin/order' => 'adminOrder/index', //actionIndex в AdminOrderController
        'admin/category/create' => 'adminCategory/create', //actionCreate в AdminCategoryController
        'admin/category/update/([0-9]+)' => 'adminCategory/update/$1', //actionUpdate в AdminCategoryController
        'admin/category/delete/([0-9]+)' => 'adminCategory/delete/$1', //actionDelete в AdminCategoryController
        'admin/category' => 'adminCategory/index', //actionIndex в AdminCategoryController
        'admin/product/create' => 'adminProduct/create', //actionCreate в AdminProductController
        'admin/product/update/([0-9]+)' => 'adminProduct/update/$1', //actionUpdate в AdminProductController
        'admin/product/delete/([0-9]+)' => 'adminProduct/delete/$1', //actionDelete в AdminProductController
        'admin/product' => 'adminProduct/index', //actionIndex в AdminProductController
        'cabinet/edit'=>'cabinet/edit',//actionEdit в CabinetController
        'cabinet'=>'cabinet/index', //actionIndex в CabinetController
        'user/logout'=>'user/logout',//actionLogout в UserController
        'user/login'=>'user/login', //actionLogin в UserController
        'user/register'=>'user/register', //actionRegister в UserController
        '404'=>'error/index', //actionIndex в ErrorController
        'gallery/([0-9]+)'=>'gallery/index/$1', //actionIndex в GalleryController
        'season/([0-9]+)/([0-9]+)'=>'season/view/$1/$2',//actionView в SeasonController
        'season/([0-9]+)'=>'season/index/$1',//actionIndex в SeasonController
        'sort-by-price-decrease'=>'product/sortd', //actionSorti в ProductController
        'sort-by-price-increase'=>'product/sorti', //actionSorti в ProductController
        'product/([0-9]+)'=>'product/view/$1',//actionView в ProductController
        'product/page-([0-9]+)'=>'product/all/$1', //actionAll в ProductController
        'product'=>'product/all/1', //actionAll в ProductController
        'category/([0-9]+)/page-([0-9]+)'=>'catalog/category/$1/$2',
        'category/([0-9]+)'=>'catalog/category/$1/1',//actionCategory в CatalogController
        'catalog'=>'catalog/index',//actionIndex в CatalogController
        'cart/checkout' => 'cart/checkout', // actionCheckOut в CartController
        'cart/delete/([0-9]+)' => 'cart/delete/$1', // actionDelete в CartController
        'cart/add/([0-9]+)' => 'cart/add/$1', // actionAdd в CartController
        'cart/addAjax/([0-9]+)' => 'cart/addAjax/$1', // actionAddAjax в CartController
        'cart'=>'cart/index', //actionIndex в CartController
        'delivery/soglashenie'=>'delivery/view',//actionView в DeliveryController
        'delivery'=>'delivery/index',//actionIndex в DeliveryController
        'about'=>'about/index', //actionIndex в AboutController
        '' => 'home/index',  //actionIndex в HomeController
        'home'=>'home/view',

    );