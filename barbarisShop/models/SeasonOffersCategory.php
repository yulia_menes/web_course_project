<?php


class SeasonOffersCategory
{
   public static function getSeasonOffersCategory(PDO $connection)
   {
       //Формулюємо SQL-запит
       $sql="SELECT * FROM season_offers";
       //Готуємо запит до виконання
       $stmt=$connection->prepare($sql);
       //Виконуємо запит
       $stmt->execute();
       //Отримуємо результат запиту в масиві
       $seasonOffersList=$stmt->fetchAll();
       //Повертаємо масив в керуючий скрипт
       return $seasonOffersList;
   }
    public static function getSeasonCategory(PDO $connection, $id)
    {
        $id=intval($id);

        if($id)
        {
            $sql="SELECT * FROM offers WHERE offers_category=".$id;
            //Готуємо запит до виконання
            $stmt=$connection->prepare($sql);
            //Виконуємо запит
            $stmt->execute();
            //Отримуємо результат запиту в масиві
            $productId=$stmt->fetchAll();
            //Повертаємо масив в керуючий скрипт
            return $productId;
        }
    }
    public static function getSeason(PDO $connection, $id)
    {
        $id=intval($id);
        if($id)
        {
            $sql="SELECT * FROM offers_category WHERE id_category='$id'";
            //Готуємо запит до виконання
            $stmt=$connection->prepare($sql);
            //Виконуємо запит
            $stmt->execute();
            //Отримуємо результат запиту в масиві
            $productId=$stmt->fetchAll();
            //Повертаємо масив в керуючий скрипт
            return $productId;
        }
    }
}
