<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>

<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">Условия доставки и оплаты букетов</h5>
        <div class="section-header-underline"></div>
    </div>
</section>
<section class="p-3 p-lg-5 d-flex flex-column accordion">
    <div class="my-auto container ">

        <div id="accordion" role="tablist" aria-multiselectable="true">

            <!-- Accordion Item 1 -->
            <div class="card">
                <div class="card-header" role="tab" id="accordionHeadingOne">
                    <div class="mb-0 row">
                        <div class="col-12 no-padding accordion-head">
                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionBodyOne" aria-expanded="false" aria-controls="accordionBodyOne"
                               class="collapsed s">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <h3>ДОСТАВКА ТОВАРА</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div id="accordionBodyOne" class="collapse" role="tabpanel" aria-labelledby="accordionHeadingOne" aria-expanded="false" data-parent="accordion">
                    <div class="card-block col-12">

                        <p>Стоимость доставки <strong>по центру</strong> и прилегающим районам составляет <strong>100 грн</strong>. При заказе от 700 грн доставка по центру осуществляется бесплатно, за исключением праздничных дней (13-15 Февраля, 6-9 Марта). Доставка по городу составляет 150 грн.
                            <br/><br/>

                            Доставка <strong>в пригород и отдаленные районы</strong> Харькова (Новая Бавария, Рогань, Северная Салтовка, Аэропорт, и т.д.) осуществляется по предварительному согласованию времени и определению стоимости доставки.
                            <br/><br/>
                            Доставка осуществляется <strong>ежедневно, без выходных с 9:00 до 22:00</strong>.
                            <br/><br/>
                            Мы можем доставить ваш букет с <strong>22:00 до 9:00</strong> утра следующего дня, стоимость такой доставки будет составлять от 200 грн.
                            <br/><br/>
                            <strong>Мы работаем с 10:00 до 20:00 с понедельника по субботу и с 11:00 до 19:00 в воскресенье</strong>, в это же время обрабатываем ваши заказы через сайт и по телефону. Оформить заказ на сайте вы можете в любое время, но обработать ваш заказ и ответить мы сможем только в рабочие часы.
                            По срочным заказам лучше звонить нам по телефону, так мы сможем быстро и качественно выполнить ваш заказ.
                            <br/><br/>
                            Если вы хотите заказать доставку день-в-день, то вам нужно оформить заказ до <strong>19:00 в будни</strong> и до <strong>18:00 в воскресенье</strong>. В зависимости от нашей загруженности, сложности и размера вашего букета, удаленности адреса - минимальное время на изготовление и доставку авторского букета 1,5-2 часа.
                            <br/><br/>
                            <strong>Как мы везем букеты?</strong>
                            <br/><br/>
                            К доставке наших букетов мы относимся так же трепетно как и к его созданию. Ваш букет аккуратно и надежно упакуют и сразу же отправят к вам, после передачи курьеру.
                            <br/><br/>
                            Все букеты упакованы по погоде: зимой тепло, а летом при необходимости во влажную упаковку. Поэтому вы можете быть спокойны за доставку.
                            <br/><br/>
                            <strong>Замена и возврат товара. Возврат денег</strong>
                            <br/><br/>
                            Если Вас не устроило качество букета или предоставляемых услуг вы можете запросить замену товара или возврат денег. С более детальными условиями можете ознакомиться в <a href="/delivery/soglashenie" class="offerta">договоре публичной оферты</a></p>
                    </div>
                </div>
            </div>

            <!-- Accordion Item 2 -->
            <div class="card">
                <div class="card-header" role="tab" id="accordionHeadingTwo">
                    <div class="mb-0 row">
                        <div class="col-12 no-padding accordion-head">
                            <a data-toggle="collapse" data-parent="#accordion" href="#accordionBodyTwo" aria-expanded="false" aria-controls="accordionBodyTwo"
                               class="collapsed s">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <h3>ЗАМЕНА И ВОЗВРАТ ТОВАРА. ВОЗВРАТ ДЕНЕГ</h3>
                            </a>
                        </div>
                    </div>
                </div>

                <div id="accordionBodyTwo" class="collapse" role="tabpanel" aria-labelledby="accordionHeadingTwo" aria-expanded="false" data-parent="accordion">
                    <div class="card-block col-12">

                        <p><strong>Покупатель может потребовать замену товара в случае если:</strong>
                            <br/>
                            - Полученный букет/композиция частично завяла или цветы выглядят не свежими
                            <br/>
                            - Полученный букет/композиция состоит из цветов, которые не были согласованы с заказчиком. Этот пункт не касается букетов, состав которых клиент оставил на усмотрение флористов.
                            <br/>
                            <br/>
                            <strong>Процедура замены товара:</strong>
                            <br/>
                            - В течении 5 часов с момента получения букета/композиции заказчик должен оформить рекламацию, заполнив форму обратной связи или написать письмо по адресу: <span class="offerta">barbaris_shop@ukr.net</span> прикрепив подтверждающие фото.
                            <br/>
                            - Когда вам будет привезён новый букет/композиция, в обязательном порядке верните курьеру предыдущий, испорченный, букет/композицию.
                            <br/>
                            <strong>Покупатель может потребовать возврат товара с компенсацией средств в следующих случаях:</strong>
                            <br/>
                            - При получении подарка клиент может сразу отказаться и вернуть заказ курьеру, указав, что букет/композиция не соответствует ожиданиям.
                            <br/>
                            - Полученный букет/композиция частично завяла или цветы выглядят не свежими.
                            <br/>
                            - Полученный букет/композиция состоит из цветов, которые не были согласованы с заказчиком. Этот пункт не касается букетов, состав которых клиент оставил на усмотрение флористов.
                            <br/>
                            <br/>
                            <strong>Процедура возврата товара с компенсацией средств:</strong>
                            <br/>
                            - В том случае, если клиент сразу при получении отказался от подарка, курьер забирает букет/композицию. Администратор интернет-магазина оформляет заявку о возврате денежных средств на счёт заказчика. Согласно правил компании, возврат денежных средств производится на протяжении 5 рабочих дней.
                            <br/>
                            - В том случае, если в течении 5 часов заказчик обратился с рекламацией о том, что букет/композиция состоит из цветов, которые не были согласованы с заказчиком, необходимо провести следующие действия: оформить рекламацию, заполнив форму обратной связи или написать письмо по адресу: bloom.flowery@gmail.com прикрепив подтверждающие фото, вернуть полученный букет/композицию.
                            <br/>
                            - Возврат денежных средств заказчику производится на протяжении 5 рабочих дней с момента рассмотрения рекламации, в том случае, если принято положительное решение.
                            <br/>
                            - В том случае, если в течении 5 часов с момента получения букета/композиции цветы завяли, необходимо провести следующие действия: оформить рекламацию, заполнив форму обратной связи или написать письмо по адресу: <span class="offerta">barbaris_shop@ukr.net</span> прикрепив подтверждающие фото, вернуть полученный букет/композицию.
                            <br/>
                            - Возврат денежных средств заказчику производится на протяжении 5 рабочих дней с момента рассмотрения рекламации, в том случае, если принято положительное решение.
                            <br/>
                            <br/>
                            <strong>Вернуть заказ можно следующими способами:</strong>
                            <br/>
                            - Вызвать курьера интернет-магазина BARBARIS, указав удобное время и адрес для приезда курьера. Выезд курьера оплачивает заказчик, согласно установленным тарифам
                            <br/>
                            - Самостоятельно привезти заказ по адресу:
                            <br/>
                            Цветочный бутик BARBARIS
                            <br/>
                            Харьков, ул. Сумская, 48
                            <br/>
                            График работы магазина:
                            <br/>
                            пн-сб 10:00 – 20:00
                            <br/>
                            вс 11:00 – 19:00
                            <br/>
                            Покупатель может потребовать возврат средств по следующим причинам:
                            <br/>
                            - Заказ по нашей вине не был доставлен в указанный день. Данный пункт не касается случаев доставки в праздничные дни, подробно смотрите в разделе «Доставка».
                            <br/>
                            - Заказчик по ошибке оплатил дважды заказ.
                            <br/>
                            <br/>
                            <strong>Процедура возврата денежных средств:</strong>
                            <br/>
                            - Возврат денежных средств заказчику производится на протяжении 5 рабочих дней с момента рассмотрения рекламации, и принятия положительного решения.
                            <br/>
                            - Возврат денежных средств производится тем же способом, которым воспользовался заказчик при оплате заказа.</p>
                    </div>
                </div>
            </div>




        </div>

    </div>
</section>
<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">Самовывоз букета</h5>
        <div class="section-header-underline"></div>
    </div>
</section>
<div class="container">
    <div class="row">
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2564.512872125711!2d36.23307401568128!3d50.00174167941592!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a0e71e4c2c7d%3A0x1a3fbcf7884fd434!2z0YPQuy4g0KHRg9C80YHQutCw0Y8sIDQ4LCDQpdCw0YDRjNC60L7Qsiwg0KXQsNGA0YzQutC-0LLRgdC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNjEwMDA!5e0!3m2!1sru!2sua!4v1607887300902!5m2!1sru!2sua" width="450" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
    <div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
        <h3>Цветочный Бутик BARBARIS</h3>
        <p class="text">Если вы хотите забрать заказ самостоятельно, то мы с удовольствием ждем вас у нас в цветочном бутике. Самовывоз возможен через 1 час после оформления заказа.
<br/><br/>
        Забрать букет вы можете по адресу:
            <br/><br/>
        улица Сумская, 48
            <br/>
        Пн-Сб: 10:00 - 20:00
            <br/>
        Вс: 10:00 - 19:00
            <br/>
        С любыми вопросами обращайтесь по телефону
            <br/>
            <br/>
            +38(066)996 73 72</p>
    </div>
    </div>
</div>
<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">Способы оплаты</h5>
        <div class="section-header-underline"></div>
    </div>
</section>
<div class="container">
    <div class="row oplata">

        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
      <img src="/img/delivery/img1_1.png">
            <h4>ОПЛАТА НАЛИЧНЫМИ</h4>
            <p>Можно оплатить наличными курьеру при<br/> получении заказа или в нашем магазине при<br/> самовывозе.</p>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
            <img src="/img/delivery/img2_2.png">
            <h4>ОПЛАТА КАРТОЙ</h4>
            <p>У нас на сайте к Оплате принимаются<br/>
                банковские карты <strong>Visa, MasterCard.</strong> Так же вы<br/>
                можете оплатить заказа через <strong>Приват24 и LiqPay.</strong></p>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
            <img src="/img/delivery/img3_3.png">
            <h4>БЕЗНАЛИЧНЫЙ РАСЧЕТ</h4>
            <p>Для удобства работы с корпоративными<br/> клиентами мы выставляем счета для<br/> безналичной оплаты.</p>
        </div>
    </div>
    <div class="row photo">
        <img src="/img/delivery/img4_4.png" class="img">
    </div>
    <div class="row section">
        <p>Вот <a href="/delivery/soglashenie" class="offerta">здесь</a> вы можете ознакомится с публичным договором оферты</p>
    </div>
</div>
<?php
include('views/include/footer.php');
?>
<style>
    .section-header-underline
    {
        border: 1px solid #222;
        width: 3rem;
        margin: 0 auto;
        margin-bottom: 30px;
    }
    body{
        background: #eee !important;
    }
    section .section-title {
        text-align: center;
        color: #203e36;
        margin-bottom: 50px;
        text-transform: uppercase;
    }
    section, .section {
        padding: 80px 0;
    }
    .accordion-head i{
        font-size: 2.5em;
        float: right;
    }
    .accordion-head h3
    {
        color:#203e36;
    }


    .accordion-head > .collapsed > i:before{
        content: "\f105";
        color:#203e36 ;
    }


    .accordion
    {
        background-color:#203e36;
    }
    .s:hover
    {
        text-decoration: none;
    }
    .fa-angle-down{
        color:#203e36;
    }
    .card-block
    {
        font-weight: 300;
        font-size: 25px;
    }
    .text
    {
        font-weight: 300;
        font-size: 20px;
    }
    .offerta
    {
        color: red;
        text-decoration: none;
    }
    .offerta:hover
    {
        color: red;
        text-decoration: none;
    }

    .oplata
    {
        text-align: center;
    }
    .photo
    {
        display: flex;
        justify-content: center;
    }
    .img
    {
        height: 200px;
    }
    .section
    {
        display: flex;
        justify-content: center;
    }

</style>
