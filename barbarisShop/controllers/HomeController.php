<?php
include_once ('models/SeasonOffersCategory.php');
include_once ('models/Gallery.php');
class HomeController
{
    public function actionIndex()
    {
        $connection=Db::connect();
        $seasonOffersCategory=SeasonOffersCategory::getSeasonOffersCategory($connection);
       $gallery=Gallery::getGallery($connection);
        require_once('views/home/home.php');
        return true;
    }
    public function actionIndexhhome()
    {
        $connection=Db::connect();
        $seasonOffersCategory=SeasonOffersCategory::getSeasonOffersCategory($connection);
        $gallery=Gallery::getGallery($connection);
        require_once('views/home/home.php');
        return true;
    }


}