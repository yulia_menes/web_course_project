<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section class="product">
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li><a href="/admin/product" class="cabinet">Управление товарами</a></li>
                    <li class="active cabinet" >Добавить товар</li>
                </ol>
            </div>
            <div class="col-12 title">
                <h4>Добавить новый товар</h4>
            </div>

            <br/>
            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>

            <div class="col-12 title">
                <div class="login-form">
                    <form action="#" method="post" enctype="multipart/form-data">
                        <table>
                            <tr>
                                <td><label for="product_name">Название товара</label></td>
                                <td><input type="text" name="name" placeholder="" value="" id="product_name"></td>
                            </tr>

                            <tr>
                                <td><label for="product_code">Артикул</label></td>
                                <td><input type="text" name="code" placeholder="" value="" id="product_code"></td>
                            </tr>

                            <tr>
                                <td><label for="product_price">Стоимость, грн</label></td>
                                <td><input type="text" name="price" placeholder="" value="" id="product_price"></td>
                            </tr>

                            <tr>
                                <td><label for="product_category">Категория</label></td>
                                <td><select name="category_id" id="product_category">
                                    <?php if (is_array($categoriesList)): ?>
                                        <?php foreach ($categoriesList as $category): ?>
                                            <option value="<?php echo $category['id']; ?>">
                                                <?php echo $category['name']; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                                </td>
                            </tr>
                            <tr>
                                <td> <label for="product_sostav">Состав</label></td>
                                <td><textarea name="sostav" id="product_sostav"></textarea></td>
                            </tr>
                            <tr>
                                <td><label for="product_image">Изображение товара</label></td>
                                <td><input type="file" name="image" placeholder="" value="" id="product_image"></td>
                            </tr>
                            <tr>
                                <td> <label for="product_color">Цвет</label></td>
                                <td><textarea name="color" id="product_color"></textarea></td>
                            </tr>

                            <tr>
                                <td> <label for="product_event">Событие</label></td>
                                <td><textarea name="event" id="product_event"></textarea></td>
                            </tr>

                            <tr>
                                <td><label for="product_flower">Цветы</label></td>
                                <td><textarea name="flower" id="product_flower"></textarea></td>
                            </tr>

                            <tr>
                                <td><label for="product_whom">Кому</label></td>
                                <td><textarea name="whom" id="product_whom"></textarea></td>
                            </tr>

                            <tr>
                                <td><label for="product_availability">Наличие на складе</label></td>
                                <td> <select name="availability">
                                        <option value="1" selected="selected">Да</option>
                                        <option value="0">Нет</option>
                                    </select></td>
                            </tr>

                            <tr>
                                <td><label for="product_new">Новинка</label></td>
                                <td> <select name="is_new">
                                        <option value="1" selected="selected">Да</option>
                                        <option value="0">Нет</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td><label for="product_status">Статус</label></td>
                                <td><select name="status">
                                        <option value="1" selected="selected">Отображается</option>
                                        <option value="0">Скрыт</option>
                                    </select></td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <input type="submit" name="submit" class="btn btn-default save" value="Сохранить">
                                </td>
                            </tr>

                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>

<style>
    body{
        background: #eee !important;
    }

    .product
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .title
    {
        display: flex;
        justify-content: center;
    }
    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        font-size: 14px;
        background: white;
        padding: 10px;
    }
    .save
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        border-radius: 10px;
        color:  black;
        background-color: white;
        background-image:
                -webkit-repeating-linear-gradient(45deg, lightgray, lightgray 1px, rgba(255,0,0,0) 2px, rgba(255,0,0,0) 10px),
                -webkit-repeating-linear-gradient(135deg, lightgray, lightgray 1px, rgba(255,0,0,0) 2px, rgba(255,0,0,0) 10px);
        box-shadow: 2px 2px 3px rgba(0,0,0,0.3);
    }
    .save:hover
    {
        background-image:
                -webkit-repeating-linear-gradient(left, lightgray, lightgray 1px, rgba(255,0,0,0) 2px, rgba(255,0,0,0) 10px);
    }
</style>