<?php

class Product
{
    const SHOW_BY_DEFAULT = 6;

    public static function getLatestProducts(PDO $connection, $page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        //Формулюємо SQL-запит
        $sql = "SELECT id,name,price, image, is_new FROM product
            WHERE status='1'
            ORDER BY id DESC LIMIT " . self::SHOW_BY_DEFAULT . " OFFSET " . $offset;
        //Готуємо запит до виконання
        $stmt = $connection->prepare($sql);
        //Виконуємо запит
        $stmt->execute();
        //Отримуємо результат запиту в масиві
        $productList = $stmt->fetchAll();
        //Повертаємо масив в керуючий скрипт
        return $productList;
    }

    public static function getProductsListByCategory(PDO $connection, $categoryId = false, $page = 1)
    {
        if ($categoryId) {
            $page = intval($page);
            $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
            //Формулюємо SQL-запит
            $sql = "SELECT id,name,price, image, is_new FROM product
             WHERE status='1' AND category_id='$categoryId'
              ORDER BY id DESC LIMIT " . self::SHOW_BY_DEFAULT . " OFFSET " . $offset;
            //Готуємо запит до виконання
            $stmt = $connection->prepare($sql);
            //Виконуємо запит
            $stmt->execute();
            //Отримуємо результат запиту в масиві
            $productList = $stmt->fetchAll();
            //Повертаємо масив в керуючий скрипт
            return $productList;
        }
    }

    public static function getProductById(PDO $connection, $id)
    {
        $id = intval($id);

        if ($id) {
            $sql = "SELECT * FROM product WHERE id=" . $id;
            //Готуємо запит до виконання
            $stmt = $connection->prepare($sql);
            //Виконуємо запит
            $stmt->execute();
            //Отримуємо результат запиту в масиві
            $productId = $stmt->fetch();
            //Повертаємо масив в керуючий скрипт
            return $productId;
        }
    }

    public static function getTotalProductInCategory(PDO $connection, $categoryId)
    {
        $sql = "SELECT count(id) AS count FROM product
            WHERE status='1' AND category_id=" . $categoryId;
        //Готуємо запит до виконання
        $stmt = $connection->prepare($sql);
        //Виконуємо запит
        $stmt->execute();
        //Отримуємо результат запиту в масиві
        $productList = $stmt->fetch();
        //Повертаємо масив в керуючий скрипт
        return $productList;
    }

    public static function getTotalProduct(PDO $connection)
    {
        $sql = "SELECT count(id) AS count FROM product
            WHERE status='1'";
        //Готуємо запит до виконання
        $stmt = $connection->prepare($sql);
        //Виконуємо запит
        $stmt->execute();
        //Отримуємо результат запиту в масиві
        $productList = $stmt->fetch();
        //Повертаємо масив в керуючий скрипт
        return $productList;
    }

    public static function getProductsByIds(PDO $connection, $idsArray)
    {
        $products = array();


        $idsString = implode(',', $idsArray);

        $sql = "SELECT * FROM product WHERE status='1' AND id IN ($idsString)";

        $result = $connection->query($sql);
        $result->setFetchMode(PDO::FETCH_ASSOC);

        $i = 0;
        while ($row = $result->fetch()) {
            $products[$i]['id'] = $row['id'];
            $products[$i]['code'] = $row['code'];
            $products[$i]['name'] = $row['name'];
            $products[$i]['price'] = $row['price'];
            $i++;
        }

        return $products;
    }

    public static function getProductsList()
    {
        // Соединение с БД
        $db = Db::connect();

        // Получение и возврат результатов
        $result = $db->query('SELECT id, name, price, code FROM product ORDER BY id ASC');
        $productsList = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $productsList[$i]['id'] = $row['id'];
            $productsList[$i]['name'] = $row['name'];
            $productsList[$i]['code'] = $row['code'];
            $productsList[$i]['price'] = $row['price'];
            $i++;
        }
        return $productsList;
    }

    public static function deleteProductById($id)
    {
        // Соединение с БД
        $db = Db::connect();

        // Текст запроса к БД
        $sql = 'DELETE FROM product WHERE id = :id';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }

    public static function updateProductById($id, $options)
    {
        // Соединение с БД
        $db = Db::connect();

        // Текст запроса к БД
        $sql = "UPDATE product
            SET 
                name = :name,
                category_id = :category_id,
                code = :code,
                price = :price,
                avallability = :availability,
                sostav= :sostav,
                color= :color,
                event= :event,
                flower= :flower,
                whom= :whom,
                status = :status,
                is_new = :is_new
            WHERE id = :id";

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':code', $options['code'], PDO::PARAM_INT);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':sostav', $options['sostav'], PDO::PARAM_STR);
        $result->bindParam(':color', $options['color'], PDO::PARAM_STR);
        $result->bindParam(':event', $options['event'], PDO::PARAM_STR);
        $result->bindParam(':flower', $options['flower'], PDO::PARAM_STR);
        $result->bindParam(':whom', $options['whom'], PDO::PARAM_STR);
        return $result->execute();
    }

    public static function createProduct($options)
    {
        // Соединение с БД
        $db = Db::connect();

        // Текст запроса к БД
        $sql = 'INSERT INTO product '
            . '(name, category_id, code, price, avallability, is_new,'
            . 'status, sostav, color, event, flower, whom)'
            . 'VALUES '
            . '(:name, :category_id, :code, :price, :availability, :is_new,'
            . ':status, :sostav, :color,:event,:flower, :whom)';

        // Получение и возврат результатов. Используется подготовленный запрос
        $result = $db->prepare($sql);
        $result->bindParam(':name', $options['name'], PDO::PARAM_STR);
        $result->bindParam(':category_id', $options['category_id'], PDO::PARAM_INT);
        $result->bindParam(':code', $options['code'], PDO::PARAM_INT);
        $result->bindParam(':price', $options['price'], PDO::PARAM_STR);
        $result->bindParam(':availability', $options['availability'], PDO::PARAM_INT);
        $result->bindParam(':is_new', $options['is_new'], PDO::PARAM_INT);
        $result->bindParam(':status', $options['status'], PDO::PARAM_INT);
        $result->bindParam(':sostav', $options['sostav'], PDO::PARAM_STR);
        $result->bindParam(':color', $options['color'], PDO::PARAM_STR);
        $result->bindParam(':event', $options['event'], PDO::PARAM_STR);
        $result->bindParam(':flower', $options['flower'], PDO::PARAM_STR);
        $result->bindParam(':whom', $options['whom'], PDO::PARAM_STR);

        if ($result->execute()) {
            // Если запрос выполенен успешно, возвращаем id добавленной записи
            return $db->lastInsertId();
        }
        // Иначе возвращаем 0
        return 0;
    }

    public static function getImage($id)
    {
        // Название изображения-пустышки
        $noImage = 'no-image.jpg';

        // Путь к папке с товарами
        $path = '/img/product/';

        // Путь к изображению товара
        $pathToProductImage = $path . $id . '.jpg';

        if (file_exists($_SERVER['DOCUMENT_ROOT'] . $pathToProductImage)) {
            // Если изображение для товара существует
            // Возвращаем путь изображения товара
            return $pathToProductImage;
        }

        // Возвращаем путь изображения-пустышки
        return $path . $noImage;
    }

    public static function Sort(PDO $connection, $page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        //Формулюємо SQL-запит
        $sql = "SELECT id,name,price, image, is_new FROM product
            WHERE status='1'
            ORDER BY price ASC LIMIT " . self::SHOW_BY_DEFAULT . " OFFSET " . $offset;
        //Готуємо запит до виконання
        $stmt = $connection->prepare($sql);
        //Виконуємо запит
        $stmt->execute();
        //Отримуємо результат запиту в масиві
        $productList = $stmt->fetchAll();
        //Повертаємо масив в керуючий скрипт

        return $productList;
    }

    public static function Sortd(PDO $connection, $page = 1)
    {
        $page = intval($page);
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        //Формулюємо SQL-запит
        $sql = "SELECT id,name,price, image, is_new FROM product
            WHERE status='1'
            ORDER BY price DESC LIMIT " . self::SHOW_BY_DEFAULT . " OFFSET " . $offset;
        //Готуємо запит до виконання
        $stmt = $connection->prepare($sql);
        //Виконуємо запит
        $stmt->execute();
        //Отримуємо результат запиту в масиві
        $productList = $stmt->fetchAll();
        //Повертаємо масив в керуючий скрипт

        return $productList;
    }




}
