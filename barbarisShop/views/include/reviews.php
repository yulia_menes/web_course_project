<!--Карточки-->
<div class="container-fluid">
    <div class="row">
        <h3 class="h">Отзывы наших клиентов</h3>
    </div>
    <div class="card-group">
        <div class="card text-center">
            <img class="card-img-top card1" src="img/home/foto1.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Алина</h5>
               <p>Спасибо за нежный и красивенный свадебный букет,он невероятный.Спасибо за уют и волшебную атмосферу в вашем стильном цветочном мире!</p>
            </div>
        </div>
        <div class="card text-center">
            <img class="card-img-top card1" src="img/home/foto7.jpg" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Анна</h5>
                <p>Спасибо цветочному бутику BARBARIS за очень нежный букет невесты. Сочетание тюльпанов и пиона завораживает. Глаз не оторвать! Спасибо!</p>
            </div>
        </div>
        <div class="card text-center">
            <img class="card-img-top card1" src="img/home/foto2.png" alt="Card image cap">
            <div class="card-body">
                <h5 class="card-title">Ирина</h5>
                <p>Букет очень понравился) теперь красуется у подружки)) мы очень довольны! Спасибо Вам!</p>
            </div>
        </div>
    </div>
</div>
<style>
</style>