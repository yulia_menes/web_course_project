
<?php if ($user['role'] == 'admin'):?>
<?php
include('views/include/header_admin.php');
?>
<div class="container cabinet">
    <div class="row title">


    </div>
    <div class="row">
        <div class="admin">
            <h3>Административная панель</h3>
            <a href="/cabinet/edit" class="cab">Редактировать данные</a>
            <h3>Вам доступны такие возможности:</h3>
            <a href="admin/product" class="cab">Управление товарами</a>
            <a href="admin/category" class="cab">Управление категориями</a>
            <a href="admin/order" class="cab">Управление заказами</a>
        </div>
        <?php
        include('views/include/footer_admin.php');
        ?>
        <?php else:?>
            <?php
            include('views/include/header.php');
            ?>
        <div class="container cabinet">
            <div class="row">
        <div class="col-12">
            <h1>Кабинет пользователя</h1>
            <h3>Мы рады приветствовать вас, <?php echo $user['name'];?>, в нашем цветочном магазине!</h3>
            <a href="/cabinet/edit" class="cab">Редактировать данные</a>
        </div>
            </div>
        </div>
            <!--Подвал-->
            <?php
            include('views/include/footer.php');
            ?>
        <?php endif;?>
    </div>
</div>

<style>
    body{
        background: #eee !important;
    }
    .title
    {
        text-align: center;
        color: #203e36;
        margin-bottom: 50px;
        text-transform: uppercase;
    }
    .cabinet
    {
        padding-top: 150px;
        padding-bottom: 200px;
        text-align: center;
    }
    .title
    {
       display: flex;
        justify-content: center;
        margin-bottom: 50px;
    }
    .cab
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        color: #45A0A4;
        background: -webkit-linear-gradient(right, #E3612C 0, #E3612C 33.3%, #FCCE30 33.3%, #FCCE30 66.6%, #58554B 66.6%, #58554B) bottom no-repeat;
        background: -o-linear-gradient(right, #E3612C 0, #E3612C 33.3%, #FCCE30 33.3%, #FCCE30 66.6%, #58554B 66.6%, #58554B) bottom no-repeat;
        background: linear-gradient(to right, #E3612C 0, #E3612C 33.3%, #FCCE30 33.3%, #FCCE30 66.6%, #58554B 66.6%, #58554B) bottom no-repeat;
        background-size: 70% 5px;
    }
    .cab:hover
    {
        color:  #203e36;;
        text-decoration: none;
        background-size: 100% 5px;
    }
    .admin
    {
        margin-left: auto;
        margin-right: auto;
    }
</style>
