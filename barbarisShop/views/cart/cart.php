<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>

<div class="container features_items ">
    <h2>Каталог</h2>
    <div class="row ">
        <?php foreach ($categories as $categoryItem):?>
            <div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-12">
                <h4><a class="category" href="/category/<?php echo $categoryItem['id'];?>">
                        <?php echo $categoryItem['name'];?>
                    </a>
                </h4>
            </div>
        <?php endforeach;?>
    </div>
    <h2>Корзина</h2>
    <div class="row cart">

        <?php if ($productsInCart): ?>
            <h2>Вы выбрали такие товары:</h2>
            <table class="table-bordered table-striped table">
                <tr>
                    <th>Код товара</th>
                    <th>Название</th>
                    <th>Стомость, грн</th>
                    <th>Количество, шт</th>
                    <th>Удалить</th>
                </tr>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?php echo $product['code'];?></td>
                        <td>
                            <a href="/product/<?php echo $product['id'];?>" class="product">
                                <?php echo $product['name'];?>
                            </a>
                        </td>
                        <td><?php echo $product['price'];?></td>
                        <td><?php echo $productsInCart[$product['id']];?></td>
                        <td>
                            <a class="btn btn-default delete" href="/cart/delete/<?php echo $product['id'];?>">
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                <tr>
                    <td colspan="4">Общая стоимость:</td>
                    <td><?php echo $totalPrice;?></td>
                </tr>

            </table>
            <a class="btn btn-default checkout" href="/cart/checkout"><i class="fa fa-shopping-cart"></i> Оформить заказ</a>
        <?php else: ?>
        <div class="container" style="padding-top: 35px">
            <h2>Корзина пуста</h2>
            <a class="btn btn-default checkout" href="/product"><i class="fa fa-shopping-cart"></i> Вернуться к покупкам</a>
        </div>
        <?php endif; ?>
    </div>
</div>
<!--Подвал-->
<?php
include('views/include/footer.php');
?>
<style>
    /*Категории*/

    .features_items
    {
        padding-top: 100px;
        text-align: center;
    }


    .category
    {

        display: flex;
        align-items: center;
        justify-content: center;
        height: 80px;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        padding: 15px 20px;
        font-size: 15px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        /*border-radius: 10px;*/
        border-style: dashed;
        color: black;
        /*background: darkgray;*/
        /*border-bottom: 5px solid black;*/
        box-shadow: 3px 3px 3px rgba(0,0,0,0.3);
    }
    .category:hover
    {
        color: black;
        text-decoration: none;
        box-shadow: 0 1px 2px rgba(0,0,0,0.3);
        text-shadow: 0 1px 0 rgba(0,0,0,0.3);
    }
    .delete
    {
        color: black;
        text-decoration: none;
    }
    .delete:hover
    {
        color:black;
        text-decoration: none;
    }

    body{
        background: #eee !important;
    }
    .product
    {
        text-decoration: none;
        color:black;
    }
    .product:hover
    {
        color: darkgreen;
    }
    .cart
    {
        padding-bottom: 100px;
    }
    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        background: white;
        padding: 10px;
    }

    .checkout {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin-right: auto;
        margin-left: auto;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        border-radius: 10px;
        color:  #36454A;
        background: darkgrey;
        box-shadow: 2px 2px 3px black;
    }
    .checkout:hover
    {
        color:  #36454A;
        text-decoration: none;
       box-shadow: 3px 3px 4px black;;
    }

</style>
