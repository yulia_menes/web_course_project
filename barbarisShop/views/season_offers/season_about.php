<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<?php foreach ($season as $seasonItem):?>
<div class="splash">
    <div class="container background">
        <div class="row">
            <div class="col-12">
        <!-- Текст, размещенный на заставке -->
        <h6><?php echo $seasonItem['restaurant'];?></h6>
        <h1 style="font-size: 34px"><?php echo $seasonItem['nazva'];?></h1>
        <h6 class="text" style="font-size: 18px"><?php echo $seasonItem['for'];?></h6>
            </div>
        </div>
    </div>
</div>
<div class="container about">
    <em><?php echo $seasonItem['description'];?></em>
</div>
<section class="container">
    <p class="section-title h4">Галерея</p>
    <div class="section-header-underline"></div>
    <div class="row gallery">
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image1'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image1'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>

        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image2'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image2'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image3'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image3'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image4'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image4'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image5'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image5'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>



        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image6'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image6'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>



        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image7'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image7'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>



        <div class="col-lg-3 col-md-4 col-xs-6 thumb">
            <a href="/img/home/season_offers/<?php echo $seasonItem['image8'];?>.JPG">
                <figure><img class="img-fluid img-thumbnail" src="/img/home/season_offers/<?php echo $seasonItem['image8'];?>.JPG" alt="Random Image"></figure>
            </a>
        </div>
    </div>
</section>
<?php endforeach;?>
<!--Подвал-->
<?php
include('views/include/footer.php');
?>
<style>
    body{
        background: #eee !important;
    }
    .splash {
        padding: 21em 9em 30em;
        <?php foreach ($season as $seasonItem):?>
        background-image: url('/img/home/season_offers/<?php echo $seasonItem['background'];?>.jpg');
        <?php endforeach;?>
        background-size: cover;
        background-attachment: fixed;
        color: black;
        text-align: center;

    }
    .background
    {
        padding: 15px;
        background-color: white;
        opacity: .7;
    }
.text{
    padding-top: 15px;
}
.about
{
    text-align: center;
    font-size: 25px;
    padding:30px;
}
    /* Start Gallery CSS */
    .thumb {
        margin-bottom: 15px;
    }
    .thumb:last-child {
        margin-bottom: 0;
    }
    /* CSS Image Hover Effects: https://www.nxworld.net/tips/css-image-hover-effects.html */
    .thumb
    figure img {
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
        -webkit-transition: .3s ease-in-out;
        transition: .3s ease-in-out;
    }
    .thumb
    figure:hover img {
        -webkit-filter: grayscale(0);
        filter: grayscale(0);
    }
    .section-title {
        text-align: center;
        color: #007b5e;
        margin-bottom: 50px;
        text-transform: uppercase;
    }
    .section-header-underline
    {
        border: 1px solid #222;
        width: 3rem;
        margin: 0 auto;
        margin-bottom: 30px;
    }
</style>

<script>
    $(document).ready(function() {
        $(".gallery").magnificPopup({
            delegate: "a",
            type: "image",
            tLoading: "Loading image #%curr%...",
            mainClass: "mfp-img-mobile",
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
            }
        });
    });

</script>