<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section class="category">
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li class="active cabinet">Управление категориями</li>
                </ol>
            </div>

            <div class="col-12 create">
                <a href="/admin/category/create" class="btn btn-default back createBtn"><i class="fa fa-plus">Добавить категорию</i></a>
            </div>
            <div class="col-12 list">
                <h4>Список категорий</h4>
            </div>

            <br/>

            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID категории</th>
                    <th>Название категории</th>
                    <th>Порядковый номер</th>
                    <th>Статус</th>
                    <th></th>
                    <th></th>
                </tr>
                <?php foreach ($categoriesList as $category):?>
                    <tr>
                        <td><?php echo $category['id'];?></td>
                        <td><?php echo $category['name'];?></td>
                        <td><?php echo $category['sort_order'];?></td>
                        <td><?php echo Category::getStatusText($category['status']); ?></td>
                        <td><a href="/admin/category/update/<?php echo $category['id'];?>" title="Редактировать">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                        <td><a href="/admin/category/delete/<?php echo $category['id'];?>" title="Удалить">
                                <i class="fa fa-times"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach;?>
            </table>

        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>

<style>
    body{
        background: #eee !important;
    }

    .category
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .create, .list
    {
        padding-top: 20px;
        display: flex;
        justify-content: center;

    }
    .createBtn
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        border-radius: 10px;
        color: black;
        background-color: white;
        background-image: -webkit-radial-gradient(white 2px, lightgray 2px);
        background-size: 12px 12px;
        box-shadow: 2px 2px 3px rgba(0,0,0,0.3);
    }
    .createBtn:hover
    {
        background-size: 16px 16px;
        color: black;
    }

    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        font-size: 14px;
        background: white;
        padding: 10px;
    }
    .fa
    {
        color:black;
    }
</style>