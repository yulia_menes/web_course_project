<?php
include_once ('models/Category.php');
include_once ('models/Product.php');

class CatalogController
{
   public function actionIndex($categoryId)
    {
        $connection=Db::connect();
        $categories=Category::getCategoriesList($connection);


        $categoryProducts=Product::getProductsListByCategory($connection,$categoryId);
        require_once('views/category/category.php');
        return true;
    }

    public function actionCategory($categoryId, $page)
    {
        $connection=Db::connect();
        $categories=Category::getCategoriesList($connection);
        $categoryProducts=Product::getProductsListByCategory($connection,$categoryId,$page);
        if(!$categoryProducts) header('Location:/404');
        $numFlowersOnThePage=Product::SHOW_BY_DEFAULT;
        $numPages = ceil(Product::getTotalProductInCategory($connection, $categoryId)['count'] / $numFlowersOnThePage);
        require_once('views/category/category.php');
        return true;
    }

    public function actionsorti($categoryId, $page)
    {
        $connection=Db::connect();
        $categories=Category::getCategoriesList($connection);
        $categoryProducts=Product::getProductsListByCategorySorti($connection,$categoryId,$page);
        if(!$categoryProducts) header('Location:/404');
        $numFlowersOnThePage=Product::SHOW_BY_DEFAULT;
        $numPages = ceil(Product::getTotalProductInCategory($connection, $categoryId)['count'] / $numFlowersOnThePage);

        require_once('views/category/category_sort.php');
        return true;
    }

}