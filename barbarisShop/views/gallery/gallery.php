<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<div class="container text">
    <?php foreach ($gallery as $galleryItem):?>
<p class="section-title h4"><?php echo $galleryItem['name'];?></p>
    <?php endforeach;?>
<div class="section-header-underline"></div>

</div>
<div class="container gallery">

    <div class="row">
        <?php foreach ($gallery as $galleryItem):?>
        <div class="rotator col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image1'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image2'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image3'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image4'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image5'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image6'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image7'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image8'];?>.JPG" alt="items photo" /></div>
        <div class="items-photo"><img src="/img/home/gallery/<?php echo $galleryItem['image9'];?>.JPG" alt="items photo" /></div>
            <?php endforeach;?>
    </div>
    </div>
</div>
<?php
include('views/include/footer.php');
?>
<style>


    * {
        box-sizing: border-box;
        margin: 0;
        padding: 0;

    }
    body { overflow-x : hidden }
    body{
        background: #eee !important;
    }
    .gallery {
        position: relative;
        width: 350px;
        /*margin: 50px auto;*/
        perspective: 1000px;
        padding-top: 100px;
        padding-bottom: 200px;
    }
    .rotator {
        position: absolute;
        left: 0;
        right: 0;
        margin: auto;
        width: 55%;
        height: 150px;
        transform-style: preserve-3d;
        animation: roter 17s linear infinite;
    }
    .rotator:hover {
        animation-play-state: paused;
    }
    @keyframes roter {
        from {
            transform: rotateY(0deg);
        }
        to {
            transform: rotateY(360deg);
        }
    }
    .items-photo {
        position: absolute;
        height: 100%;
        width: 100%;
        overflow: hidden;
        border: 2px solid #333;
    }
    .items-photo:hover img {
        transform: scale(1.2);
    }
    .items-photo img {
        height: 100%;
        width: 100%;
        transition: all 3s ease;
    }
    .items-photo:first-child {
        transform: rotateY(calc(40deg * 1)) translateZ(300px);
    }
    .items-photo:nth-child(2) {
        transform: rotateY(calc(40deg * 2)) translateZ(300px);
    }
    .items-photo:nth-child(3) {
        transform: rotateY(calc(40deg * 3)) translateZ(300px);
    }
    .items-photo:nth-child(4) {
        transform: rotateY(calc(40deg * 4)) translateZ(300px);
    }
    .items-photo:nth-child(5) {
        transform: rotateY(calc(40deg * 5)) translateZ(300px);
    }
    .items-photo:nth-child(6) {
        transform: rotateY(calc(40deg * 6)) translateZ(300px);
    }
    .items-photo:nth-child(7) {
        transform: rotateY(calc(40deg * 7)) translateZ(300px);
    }
    .items-photo:nth-child(8) {
        transform: rotateY(calc(40deg * 8)) translateZ(300px);
    }
    .items-photo:nth-child(9) {
        transform: rotateY(calc(40deg * 9)) translateZ(300px);
    }

    .section-title {
        text-align: center;
        color: #203e36;
        margin-bottom: 50px;
        text-transform: uppercase;
    }
    .section-header-underline
    {
        border: 1px solid #222;
        width: 3rem;
        margin: 0 auto;
        margin-bottom: 30px;
    }
.text
{
    padding-top: 100px;
}
</style>
