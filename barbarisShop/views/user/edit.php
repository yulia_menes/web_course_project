<?php
include('views/include/header.php');
?>
<div class="container edit">
    <div class="row">
        <?php if ($result): ?>
            <p style="text-align: center" class="result">Данные отредактированны!</p>
        <?php else: ?>
        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li> - <?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>


            <form class="transparent" action="#" method="post">
                <div class="form-inner1">
                    <h3>Редактирование данных</h3>
                    <label for="username">Имя пользователя</label>
                    <input type="text" id="username" name="name" value="<?php echo $name;?>" required>
                    <label for="password">Пароль</label>
                    <input id="password" type="password" name="password" value="<?php echo $password;?>" required>

                    <input type="submit" value="Сохранить" name="submit">
                </div>
            </form>
        </div>
        <?php endif; ?>
    </div>
</div>

<!--Подвал-->
<?php
include('views/include/footer.php');
?>
<style>
    body {
        background-color: #6d6875;
        background-image:
                linear-gradient(135deg, rgba(109,104,117, 0.2) 0%, rgba(255,180,162, 0.1) 25%, rgba(109,104,117, 0.2) 50%, rgba(255,180,162, 0.1) 75%, rgba(109,104,117, 0.2) 100%),
                linear-gradient(90deg, #6d6875, #b5838d, #e5989b, #ffb4a2, #ffcdb2);
        background-size: 6px 6px, 100%;
    }
    /* * {box-sizing: border-box;}*/
    .edit
    {

        margin-right: auto;
        margin-left: auto;
        padding-top: 120px;
        padding-bottom: 70px;
        display: flex;
        justify-content: center;
    }
    .transparent {
        width: 800px;
        position: relative;
        max-width: 450px;
        padding: 60px 50px;
        /*margin: 50px auto 0;*/
        background-image: url(https://html5book.ru/wp-content/uploads/2017/01/photo-roses.jpg);
        background-size: cover;
    }
    .transparent:before {
        content: "";
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background: linear-gradient(to right bottom,rgba(43, 44, 78, .5),rgba(104, 22, 96, .5));
    }
    .form-inner1 {position: relative;}
    .form-inner1 h3 {
        position: relative;
        margin-top: 0;
        color: white;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
        font-size: 26px;
        text-transform: uppercase;
    }
    .form-inner1 h3:after {
        content: "";
        position :absolute;
        left: 0;
        bottom: -6px;
        height: 2px;
        width: 60px;
        background: #1762EE;
    }
    .form-inner1 label {
        display: block;
        padding-left: 15px;
        font-family: 'Roboto', sans-serif;
        color: rgba(255,255,255,.6);
        text-transform: uppercase;
        font-size: 14px;
    }
    .form-inner1 input {
        display: block;
        width: 100%;
        padding: 0 15px;
        margin: 10px 0 15px;
        border-width: 0;
        line-height: 40px;
        border-radius: 20px;
        color: white;
        background: rgba(255,255,255,.2);
        font-family: 'Roboto', sans-serif;
    }


    .form-inner1 input[type="submit"] {background: slateblue;}

    .result
    {
        font-size: 25px;
    }
</style>