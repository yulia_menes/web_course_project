<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section class="category">
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li><a href="/admin/order" class="cabinet">Управление заказами</a></li>
                    <li class="active cabinet">Редактировать заказ</li>
                </ol>
            </div>
            <div class="col-12 list">
                <h4>Редактировать заказ №<?php echo $id; ?></h4>
            </div>

            <br/>
            <div class="col-lg-12 title">
                <div class="login-form">
                    <form action="#" method="post">
                      <table>
                          <tr>
                          <td>Имя клиента</td>

                          <td><input type="text" name="userName" placeholder="" value="<?php echo $order['user_name']; ?>"></td>
                          </tr>

                          <tr>
                        <td>Телефон клиента</td>
                              <td><input type="text" name="userPhone" placeholder="" value="<?php echo $order['user_phone']; ?>"></td>
                          </tr>

                          <tr>
                        <td>Комментарий клиента</td>
                              <td><input type="text" name="userComment" placeholder="" value="<?php echo $order['user_comment']; ?>"></td>
                          </tr>

                          <tr>
                        <td>Дата оформления заказа</td>
                              <td><input type="text" name="date" placeholder="" value="<?php echo $order['date']; ?>"></td>

                          </tr>

                          <tr>
                        <td>Статус</td>
                        <td>
                              <select name="status">
                            <option value="1" <?php if ($order['status'] == 1) echo ' selected="selected"'; ?>>Новый заказ</option>
                            <option value="2" <?php if ($order['status'] == 2) echo ' selected="selected"'; ?>>В обработке</option>
                            <option value="3" <?php if ($order['status'] == 3) echo ' selected="selected"'; ?>>Доставляется</option>
                            <option value="4" <?php if ($order['status'] == 4) echo ' selected="selected"'; ?>>Закрыт</option>
                        </select>
                        </td>
                          </tr>
                          <tr>
                              <td colspan="2">
                        <input type="submit" name="submit" class="btn btn-default" value="Сохранить">
                              </td>
                          </tr>
                      </table>
                    </form>
                </div>
            </div>

        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>
<style>
    body{
        background: #eee !important;
    }

    .category
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .list
    {
        padding-top: 20px;
        display: flex;
        justify-content: center;

    }
    .title
    {
        display: flex;
        justify-content: center;
    }
    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        font-size: 14px;
        background: white;
        padding: 10px;
    }
    .fa
    {
        color:black;
    }
</style>
