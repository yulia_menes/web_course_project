<?php


class User
{
        public static function register($name, $email, $password)
        {
          $db=Db::connect();
          $sql='INSERT INTO user (name, email, password)'
              . 'VALUES (:name, :email, :password)';
          $result=$db->prepare($sql);
          $result->bindParam(':name', $name, PDO::PARAM_STR);
          $result->bindParam(':email', $email, PDO::PARAM_STR);
          $result->bindParam(':password', $password, PDO::PARAM_STR);

          return $result->execute();
        }

        //Проверяем существует ли пользователь с заданным email и password
        public static function checkUserData($email, $password)
        {
            $db=Db::connect();

            $sql='SELECT * FROM user WHERE email=:email AND password=:password';
            $result=$db->prepare($sql);
            $result->bindParam(':email', $email, PDO::PARAM_STR);
            $result->bindParam(':password', $password, PDO::PARAM_STR);
            $result->execute();

            $user=$result->fetch();
            if($user)
            {
                return $user['id'];
            }
            return false;
        }

        //Запоминаем пользователя
        public static function auth($userId)
        {

            $_SESSION['user']=$userId;
        }

        public static function checkLogged()
        {

            //Если сессия есть вернем идентификатор пользователя
            if(isset($_SESSION['user']))
            {
                return $_SESSION['user'];
            }
            header("Location: /user/login");
        }

        public static function isGuest()
        {

            if(isset($_SESSION['user']))
            {
                return false;
            }
            return true;

        }

        public static function checkName($name)
        {
            if(preg_match_all("/^[a-zA-Zа-яёА-ЯЁ]+$/u", $name))
            {
                return true;
            }
            return false;
           /* if(strlen($name)>=2)
            {
                return true;
            }
            return false;*/
        }

        public static function checkPassword($password)
        {
            if(preg_match_all("/^\S*(?=\S{6,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])\S*$/D", $password))
            {
                return true;
            }
            return false;
            /*if(strlen($password)>=6)
            {
                return true;
            }
            return false;*/
        }

        public static function checkEmail($email)
        {
            if(preg_match_all("([A-Za-z]+[\.A-Za-z0-9_-]*[A-Za-z0-9]+@[A-Za-z]+\.[A-Za-z]+)", $email))
            {
                return true;
            }
            return false;
            /*if(filter_var($email, FILTER_VALIDATE_EMAIL))
            {
                return true;
            }
            return false;*/
        }
    public static function checkPhone($phone)
    {
        if(preg_match_all("([+380]+\d{9})", $phone))
        {
            return true;
        }
        return false;
        /*if (strlen($phone) >= 10) {
            return true;
        }
        return false;*/
    }

        public static function checkEmailExists($email) {

            $db = Db::connect();

            $sql = 'SELECT COUNT(*) FROM user WHERE email = :email';

            $result = $db->prepare($sql);
            $result->bindParam(':email', $email, PDO::PARAM_STR);
            $result->execute();

            if($result->fetchColumn())
                return true;
            return false;
        }

        public static function getUserById($id)
        {
            if($id)
            {
                $db=Db::connect();
                $sql='SELECT * FROM user WHERE id=:id';

                $result=$db->prepare($sql);
                $result->bindParam(':id',$id, PDO::PARAM_INT);

                //Указываем, что хотим получить данные из массива
                $result->setFetchMode(PDO::FETCH_ASSOC);
                $result->execute();

                return $result->fetch();
            }
        }
    public static function edit($id, $name, $password)
    {
        $db = Db::connect();

        $sql = "UPDATE user 
            SET name = :name, password = :password 
            WHERE id = :id";

        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':password', $password, PDO::PARAM_STR);
        return $result->execute();
    }


}