<div class="container">
 <div class="row text">
 <h3 >Наши контакты</h3>
 </div>
 <div class="row">
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
  <p class="footer">Цветочный Бутик BARBARIS<br/>
     Харьков, ул. Киевская, 48<br/>
     Пн-Сб: 10:00 - 20:00<br/>
     Вс: 11:00 - 19:00<br/>
     +38 098 996 73 72</p>
      <br/><br/><br/><br/>
      <p class="footer">
          Кофейня BARBARIS<br/>
          Харьков, ул. Киевская, 48<br/>
          Пн-Сб: 09:00 - 20:00<br/>
          Вс: 10:00 - 20:00<br/>
          +38 098 996 73 72</p>
  </div>
  <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 footer1">
      <form class="decor" action="https://formspree.io/f/mbjpzqdy"
            method="POST">
          <div class="form-left-decoration"></div>
          <div class="form-right-decoration"></div>
          <div class="circle"></div>
          <div class="form-inner">
              <h3>Написать нам</h3>
              <input type="text" placeholder="Имя" name="name" required>
              <input type="email" placeholder="Почта" name="email" required>
              <textarea placeholder="Сообщение..." name="message" rows="3"required></textarea>
              <input type="hidden" name="_subject" value="Новое сообщение!">
              <input type="text" name="_gotcha" style="display: none">
              <input type="submit" value="Отправить" name="submit">
          </div>
      </form>
  </div>
 </div>
 <div class="row sos">
 <ul class="socialIcons">
  <li><a class="facebook" href="#"><i class="fa fa-facebook"></i></a></li>
  <li><a class="twitter" href="#"><i class="fa fa-twitter"></i></a></li>
  <li><a class="instagram" href="#"><i class="fa fa-instagram"></i></a></li>
  <li><a class="youtube" href="#"><i class="fa fa-youtube"></i></a></li>
</ul>
 </div>
</div>

</body>
</html>
<style>

    .footer1
    {
        display: flex;
        justify-content: center;
    }
.footer
{
    text-align:center;

}
.text h3
{
    margin-left: auto;
    margin-right: auto;
}
.socialIcons {
  padding: 0;
  margin: 0;
  list-style: none;
  display: inline-block;
 
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  margin: auto;
  height: 90px;
  text-align: center;
}
.socialIcons li {
  display: inline-block;
  margin: auto 10px;
}
.socialIcons li a {
  text-align: center;
  display: inline-block;
  font-size: 4rem;
  height: 100px;
  width: 100px;
  line-height: 100px;
  color: darkgray;
  transition: all .5s ease;
  transform-origin: left;
  transform: rotateY(-30deg);
  text-shadow: 10px 0px 1px #131313, 10px 0px 1px #070707, 10px 0px 1px black, 10px 0px 1px black;
}
.socialIcons li a:hover {
  color: #202020;
  transform: rotate(0deg);
  text-shadow: 0px 0px 1px #131313, 0px 0px 1px #070707, 0px 0px 1px black, 0px 0px 1px black;
}
/*Форма*/
.decor {
    position: relative;
    max-width: 400px;
    /*margin: 50px auto 0;*/
    background: darkgrey;
    border-radius: 30px;
}
.form-left-decoration,
.form-right-decoration {
    content: "";
    position: absolute;
    width: 50px;
    height: 20px;

    border-radius: 20px;
}
.form-left-decoration {
    bottom: 60px;
    left: -30px;
}
.form-right-decoration {
    top: 60px;
    right: -30px;
}
.form-left-decoration:before,
.form-left-decoration:after,
.form-right-decoration:before,
.form-right-decoration:after {
    content: "";
    position: absolute;
    width: 50px;
    height: 20px;
    border-radius: 30px;
    background: darkgrey;
}
.form-left-decoration:before {top: -20px;}
.form-left-decoration:after {
    top: 20px;
    left: 10px;
}
.form-right-decoration:before {
    top: -20px;
    right: 0;
}
.form-right-decoration:after {
    top: 20px;
    right: 10px;
}
.circle {
    position: absolute;
    bottom: 80px;
    left: -55px;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    background: darkgrey;
}
.form-inner {padding: 50px;}
.form-inner input,
.form-inner textarea {
    display: block;
    width: 100%;
    padding: 0 20px;
    margin-bottom: 10px;
    background: #E9EFF6;
    line-height: 40px;
    border-width: 0;
    border-radius: 20px;
    font-family: 'Roboto', sans-serif;
}
.form-inner input[type="submit"] {
    margin-top: 30px;
    background: #E9EFF6;
    border-bottom: 4px solid black;
    color: gray;
    font-size: 14px;
}
.form-inner textarea {resize: none;}
.form-inner h3 {
    margin-top: 0;
    font-family: 'Roboto', sans-serif;
    font-weight: 500;
    font-size: 24px;
    color:white;
}
</style>
