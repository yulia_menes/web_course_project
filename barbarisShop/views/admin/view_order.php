<?php
include('views/include/header_admin.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section class="category">
    <div class="container">
        <div class="row">
            <br/>
            <div class="breadcrumbs col-12">
                <ol class="breadcrumb">
                    <li><a href="/cabinet" class="cabinet">Админпанель</a></li>
                    <li><a href="/admin/order" class="cabinet">Управление заказами</a></li>
                    <li class="active cabinet">Просмотр заказа</li>
                </ol>
            </div>
            <div class="col-12 list">
                <h4>Просмотр заказа №<?php echo $order['id']; ?></h4>
            </div>

            <br/>
            <div class="col-12 list">
            <h5>Информация о заказе</h5>
            </div>
            <div class="col-12 list">
            <table>
                <tr>
                    <td>Номер заказа</td>
                    <td><?php echo $order['id']; ?></td>
                </tr>
                <tr>
                    <td>Имя клиента</td>
                    <td><?php echo $order['user_name']; ?></td>
                </tr>
                <tr>
                    <td>Телефон клиента</td>
                    <td><?php echo $order['user_phone']; ?></td>
                </tr>
                <tr>
                    <td>Комментарий клиента</td>
                    <td><?php echo $order['user_comment']; ?></td>
                </tr>
                <?php if ($order['user_id'] != 0): ?>
                    <tr>
                        <td>ID клиента</td>
                        <td><?php echo $order['user_id']; ?></td>
                    </tr>
                <?php endif; ?>
                <tr>
                    <td><b>Статус заказа</b></td>
                    <td><?php echo Order::getStatusText($order['status']); ?></td>
                </tr>
                <tr>
                    <td><b>Дата заказа</b></td>
                    <td><?php echo $order['date']; ?></td>
                </tr>
            </table>
            </div>
            <div class="col-12 list">
            <h5>Товары в заказе</h5>
            </div>
            <div class="col-12 list">
            <table class="table-admin-medium table-bordered table-striped table ">
                <tr>
                    <th>ID товара</th>
                    <th>Артикул товара</th>
                    <th>Название</th>
                    <th>Цена</th>
                    <th>Количество</th>
                </tr>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?php echo $product['id']; ?></td>
                        <td><?php echo $product['code']; ?></td>
                        <td><?php echo $product['name']; ?></td>
                        <td><?php echo $product['price']; ?> грн</td>
                        <td><?php echo $productsQuantity[$product['id']]; ?></td>
                    </tr>
                <?php endforeach; ?>
            </table>
                </div>
            <a href="/admin/order/" class="btn btn-default back"><i class="fa fa-arrow-left"></i> Назад</a>
        </div>
    </div>
</section>
<?php
include('views/include/footer_admin.php');
?>
<style>
    body{
        background: #eee !important;
    }

    .category
    {
        padding-top: 100px;
    }
    .breadcrumb
    {
        background-color: white;
    }

    .cabinet
    {
        margin-left: 15px;
        text-decoration: none;
        color:black;
    }
    .cabinet:hover
    {
        color:black;
    }
    .list
    {
        padding-top: 20px;
        display: flex;
        justify-content: center;

    }


    table {
        font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
        text-align: center;
        border-collapse: separate;
        border-spacing: 5px;
        background: #ECE9E0;
        color: #656665;
        border: 16px solid #ECE9E0;
        border-radius: 20px;
    }
    th {
        font-size: 15px;
        padding: 10px;
    }
    td {
        font-size: 14px;
        background: white;
        padding: 10px;
    }
    .fa
    {
        color:black;
    }
    .back
    {
        text-decoration: none;
        display: inline-block;
        padding: 5px 10px;
        letter-spacing: 1px;
        margin: 0 20px;
        font-size: 24px;
        transition: .3s ease-in-out;
        color: black;
        line-height: 1.2;
        position: relative;
        text-transform: uppercase;
    }
    .back:after
    {
        content: "";
        height: 100%;
        min-width: 4px;
        background: black;
        position: absolute;
        left: 0;
        bottom: 0;
        transition: .5s;
    }
    .back:hover:after
    {
        min-width: 100%;
        background: #95a5a6;
        opacity: .35;
    }
    .back:hover
    {
        color: black;
        text-decoration: none;
    }
</style>