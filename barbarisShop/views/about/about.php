<?php
include('views/include/header.php');
?>
<?php
include('views/include/returnToTopArrow.php');
?>
<section id="team" class="pb-5">
    <div class="container">
        <h5 class="section-title h1">НАША КОМАНДА</h5>
        <div class="section-header-underline"></div>
        <div class="row">
            <!-- Team member -->
            <?php foreach ($team as $teamId):?>
            <div class="col-xl-4 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="image-flip" ontouchstart="this.classList.toggle('hover');">
                    <div class="mainflip">
                        <div class="frontside">
                            <div class="card">
                                <div class="card-body text-center">
                                    <p><img class=" img-fluid" src="/img/about/<?php echo $teamId['image'];?>.png" alt="card image"></p>
                                    <h4 class="card-title"><?php echo $teamId['name_surname'];?></h4>
                                    <p class="card-text"><?php echo $teamId['about_florist'];?></p>
                                    <a href="#" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="backside">
                            <div class="card">
                                <div class="card-body text-center mt-4">
                                    <h4 class="card-title"><?php echo $teamId['name_surname'];?></h4>
                                    <p class="card-text"><?php echo $teamId['about_opis'];?></p>
                                    <ul class="list-inline">
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-facebook"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-twitter"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-skype"></i>
                                            </a>
                                        </li>
                                        <li class="list-inline-item">
                                            <a class="social-icon text-xs-center" target="_blank" href="#">
                                                <i class="fa fa-google"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach;?>

        </div>
    </div>

<!-- Team -->

<!--Video Player-->
<div class="container" >
<div class="content">
    <h1 class="section-title h1">Видео, которые могут быть Вам интересны</h1>
    <div class="section-header-underline"></div>
    <div class="row">
    <div class="video-gallery">
        <div class="gallery-item ">
            <img src="/img/about/video_photo1.jpg" alt="" />
            <div class="gallery-item-caption">
                <div>
                    <h2>10 глупых вопросов флористу</h2>
                    <p>Мы не начто не намикаем конечно, но...</p>
                </div>
                <a class="vimeo-popup" href="https://www.youtube.com/watch?v=hN1XxpePOao&ab_channel=%D0%96%D0%98%D0%97%D0%90"></a>
            </div>
        </div>

        <div class="gallery-item ">
            <img src="/img/about/video_photo2.jpg" alt="" />
            <div class="gallery-item-caption">
                <div>
                    <h2>Как собрать красивый букет</h2>
                    <p>Полезные советы</p>
                </div>
                <a class="vimeo-popup" href="https://www.youtube.com/watch?v=drisNyogaus&ab_channel=KatrinaFlowers"></a>
            </div>
        </div>

        <div class="gallery-item">
            <img src="/img/about/video_photo3.jpg" alt="" />
            <div class="gallery-item-caption">
                <div>
                    <h2>Уход за срезанными цветами</h2>
                    <p>Советы флористов</p>
                </div>
                <a class="vimeo-popup" href="https://www.youtube.com/watch?v=rdJNLDsucaM&ab_channel=%D0%A1%D0%B5%D0%BC%D0%B8%D1%86%D0%B2%D0%B5%D1%82%D0%B8%D0%BA.%D0%9E%D1%82%D1%82%D0%B5%D0%BD%D0%BA%D0%B8%D0%BD%D0%B0%D1%88%D0%B8%D1%85%D1%87%D1%83%D0%B2%D1%81%D1%82%D0%B2"></a>
            </div>
        </div>

        <div class="gallery-item">
            <img src="/img/about/video_photo4.jpg" alt="" />
            <div class="gallery-item-caption">
                <div>
                    <h2>Упаковка букета</h2>
                    <p>Интересные способы</p>
                </div>
                <a class="vimeo-popup" href="https://www.youtube.com/watch?v=azCy7O9_B6Y&ab_channel=KatrinaFlowers"></a>
            </div>
        </div>
    </div>
    </div>
</div>
</div>
</section>

<!--Подвал-->
<?php
include('views/include/footer.php');
?>
<style>
    /*Команда*/
    .section-header-underline
    {
        border: 1px solid #222;
        width: 3rem;
        margin: 0 auto;
        margin-bottom: 30px;
    }
    body{
        background: #eee !important;
    }

    .btn-primary:hover,
    .btn-primary:focus {
        background-color: #108d6f;
        border-color: #108d6f;
        box-shadow: none;
        outline: none;
    }

    .btn-primary {
        color: #fff;
        background-color: #007b5e;
        border-color: #007b5e;
    }

    section {
        padding: 80px 0;
    }

    section .section-title {
        text-align: center;
        color: #203e36;
        margin-bottom: 50px;
        text-transform: uppercase;
    }

    #team .card {
        border: none;
        background: #ffffff;
    }

    .image-flip:hover .backside,
    .image-flip.hover .backside {
        -webkit-transform: rotateY(0deg);
        -moz-transform: rotateY(0deg);
        -o-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        transform: rotateY(0deg);
        border-radius: .25rem;
    }

    .image-flip:hover .frontside,
    .image-flip.hover .frontside {
        -webkit-transform: rotateY(180deg);
        -moz-transform: rotateY(180deg);
        -o-transform: rotateY(180deg);
        transform: rotateY(180deg);
    }

    .mainflip {
        -webkit-transition: 1s;
        -webkit-transform-style: preserve-3d;
        -ms-transition: 1s;
        -moz-transition: 1s;
        -moz-transform: perspective(1000px);
        -moz-transform-style: preserve-3d;
        -ms-transform-style: preserve-3d;
        transition: 1s;
        transform-style: preserve-3d;
        position: relative;
    }

    .frontside {
        position: relative;
        -webkit-transform: rotateY(0deg);
        -ms-transform: rotateY(0deg);
        z-index: 2;
        margin-bottom: 30px;
    }

    .backside {
        position: absolute;
        top: 0;
        left: 0;
        background: white;
        -webkit-transform: rotateY(-180deg);
        -moz-transform: rotateY(-180deg);
        -o-transform: rotateY(-180deg);
        -ms-transform: rotateY(-180deg);
        transform: rotateY(-180deg);
        -webkit-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        -moz-box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
        box-shadow: 5px 7px 9px -4px rgb(158, 158, 158);
    }

    .frontside,
    .backside {
        -webkit-backface-visibility: hidden;
        -moz-backface-visibility: hidden;
        -ms-backface-visibility: hidden;
        backface-visibility: hidden;
        -webkit-transition: 1s;
        -webkit-transform-style: preserve-3d;
        -moz-transition: 1s;
        -moz-transform-style: preserve-3d;
        -o-transition: 1s;
        -o-transform-style: preserve-3d;
        -ms-transition: 1s;
        -ms-transform-style: preserve-3d;
        transition: 1s;
        transform-style: preserve-3d;
    }

    .frontside .card,
    .backside .card {
        min-height: 312px;
    }

    .backside .card a {
        font-size: 18px;
        color: #007b5e !important;
    }

    .frontside .card .card-title,
    .backside .card .card-title {
        color: #007b5e !important;
    }

    .frontside .card .card-body img {
        width: 120px;
        height: 120px;
        border-radius: 50%;
    }
    /*Видео*/
    .section-header {

        text-align: center;
        color: #007b5e;
        margin-bottom: 50px;
        text-transform: uppercase;
    }


    .video-gallery {
        position: relative;
        margin: 0 auto;
        max-width: 1000px;
        text-align: center;
    }

    .video-gallery .gallery-item {
        position: relative;
        float: left;
        overflow: hidden;
        margin: 10px 1%;
        min-width: 320px;
        max-width: 580px;
        max-height: 360px;
        width: 48%;
        background: #000;
        cursor: pointer;
    }

    .video-gallery .gallery-item img {
        position: relative;
        display: block;
        opacity: .45;
        width: 105%;
        height: 300px;
        transition: opacity 0.35s, transform 0.35s;
        transform: translate3d(-23px, 0, 0);
        backface-visibility: hidden;
    }

    .video-gallery .gallery-item .gallery-item-caption {
        padding: 2em;
        color: #fff;
        text-transform: uppercase;
        font-size: 1.25em;
    }

    .video-gallery .gallery-item .gallery-item-caption,
    .video-gallery .gallery-item .gallery-item-caption > a {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
    }

    .video-gallery .gallery-item h2 {
        font-weight: 300;
        overflow: hidden;
        padding: 0.5em 0;
    }


    .video-gallery .gallery-item h2,
    .video-gallery .gallery-item p {
        position: relative;
        margin: 0;
        z-index: 10;
    }

    .video-gallery .gallery-item p {
        letter-spacing: 1px;
        font-size: 68%;

        padding: 1em 0;
        opacity: 0;
        transition: opacity 0.35s, transform 0.35s;
        transform: translate3d(10%, 0, 0);
    }

    .video-gallery .gallery-item:hover img {
        opacity: .3;
        transform: translate3d(0, 0, 0);

    }

    .video-gallery .gallery-item .gallery-item-caption {
        text-align: left;
    }

    .video-gallery .gallery-item h2::after {
        content: "";
        position: absolute;
        bottom: 0;
        left: 0;
        width: 15%;
        height: 1px;
        background: #fff;

        transition: transform 0.3s;
        transform: translate3d(-100%, 0, 0);
    }

    .video-gallery .gallery-item:hover h2::after {
        transform: translate3d(0, 0, 0);
    }

    .video-gallery .gallery-item:hover p {
        opacity: 1;
        transform: translate3d(0, 0, 0);
    }

    @media screen and (max-width: 50em) {
        .video-gallery .gallery-item {
            display: inline-block;
            float: none;
            margin: 10px auto;
            width: 100%;
        }
    }

</style>
<script>
    $(document).ready(function() {
        $('.video-gallery').magnificPopup({
            delegate: 'a',
            type: 'iframe',
            gallery:{
                enabled:true
            }
        });
    });
</script>