<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
           
        <img class="d-block w-100" src="img/home/carousel1.jpg" alt="Второй слайд">
            <div class="carousel-caption d-none d-md-block d-block">
                <h3 id="text" class="color">Посмотрите ассортимент</h3>
                <p id="text" class="color">Мы уверенны, что Вам понравиться!</p>
                <a href="/product" id="as" class="one" id="text">Посмотреть ассортимент</a>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="img/home/carousel2.jpg" alt="Второй слайд">
            <div class="carousel-caption d-none d-md-block d-block">
                <h3 id="text">А как вам наша галерея?</h3>
                <p id="text">Успейте посмотреть именно сейчас!</p>
                <a href="/home#gallery" id="as" class="one" id="text">Открыть галерею</a>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="img/home/carousel3.jpg" alt="Третий слайд">
            <div class="carousel-caption d-none d-md-block d-block">
                <h3 id="text">Узнайте все о наших мастерах</h3>
                <p id="text">У нас только профессионалы</p>
                <a href="/about" id="as" class="one" id="text">Посмотреть</a>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>

<style>
    .carousel-caption
    {
        bottom: auto;
        top:40%;
        height: 175px;
        background-color: darkgray;
        opacity: 0.9;
    }


    #text, #as
    {
        font-weight: bold;
        font-size: 18px;
        font-family: 'Open Sans',Arial,sans-serif;
        color:white;
        text-shadow:2px 2px 2px black;
    }


    .one
    {
        display: inline-block;
        text-decoration: none;
        text-transform: uppercase;
        letter-spacing: 1px;
        margin: 15px 25px;
        padding: 15px 20px;
        font-size: 20px;
        font-weight: bold;
        font-family: 'Montserrat', sans-serif;
        transition: 0.4s ease-in-out;
        background-color: darkgray;


    }
    #as
    {
        box-sizing: border-box;
        border: 2px solid white;
        color: white;
        font-size: 12px;
        font-style: italic;
        font-weight: bold;
        letter-spacing: 1.2px;
        line-height: 1;
        padding: 16px 40px;
        position: relative;

    }
    #as:after
    {
        content: "";
        display: block;
        border: 2px solid white;
        position: absolute;
        top: -7px;
        right: 5px;
        bottom: -7px;
        left: 5px;
    }
</style>
